#!/usr/bin/python
#-*- coding: utf-8 -*-

#import all other necessary modules
from __future__ import division
from math import pi,degrees,sin,cos,sqrt
#import locale
import numpy as np
from prettytable import PrettyTable
import sys
import csv
import os

debug=0
time=True
enable_terminal_output = 0
optimise_interface = 0
calculate_all_torques = 1

if time:
  import time

if optimise_interface != 1:
  import matplotlib as mpl
  import matplotlib.pyplot as plt
  from matplotlib.patches import Rectangle, Wedge
  from glob import glob  
  
  




#locale.setlocale(locale.LC_NUMERIC,'de_DE')


if optimise_interface != 1:
  save_figs=False
  show_figs=False
  #show_figs=False
  draw_coils = True
  plot_PM = True
  plot_AR = True
  presentation=False
  linear_repr=False   #Draw the RFAPM machine in a linearised representational format
  plot_contour=True
  plot_current_density=False #dont think these mathematical functions are correct for the quasi-halbach machine
  #plot_km_range=False
  plot_approx=True
  plot_harmonics=True
  plot_total=False     #Plot the analysis for the PM + the windings
  plot_torque=True
  plot_Tm_Lorentz=True
  plot_Tm_Lorentz_simple=True
  
  #########################
  # Contour Plot settings #
  #########################

  contour_figsize=(6,8)
  colorbar_shrink=0.25
  colorbar_aspect=10  

  LINEWIDTH=1



READ_INPUT = 1
MANUAL_INPUT = 2
input_method = READ_INPUT
Type='II'

MAGNETS_ONLY = 1
WINDINGS_ONLY = 2
ALL_ACTIVE = 3

pretty_terminal_table = PrettyTable(['SEMFEM Output Variable','Value','Unit'])
pretty_terminal_table.align = 'l'
pretty_terminal_table.border= True  
has_opened = False
enable_variable_test = False

def addToTable (variable_string_name, value, unit_string, enable_disable, dir_parent):
  if enable_disable == True:
    pretty_terminal_table.add_row([variable_string_name,value,unit_string])
    variable_string_name = variable_string_name.replace(" ", "_")
    with open(dir_parent+'script_results/qhdrfapm/output.csv',"a") as output_dat:
      output_dat.write('%s,%f\n'%(variable_string_name,value))

def main(*args):
  if time:
    start_time = time.time()
  if __name__ != '__main__':
    VERBOSITY=0		#VERBOSITY setting when this script is invoked by an other script
    #display_drawings=0
    dir_parent = './mp/p%s/'%args[1]
    number_of_threads = int(args[1])
  else:
    VERBOSITY=1		#VERBOSITY setting when this script is executed by itself
    #display_drawings=0  
    dir_parent = ''
    number_of_threads = 1  
  
  input_data = np.genfromtxt(args[0],skip_header=1,delimiter=',',usecols=(1),unpack=True)
  input_data_original = np.genfromtxt('input_original.csv',skip_header=1,delimiter=',',usecols=(1),unpack=True)
  
  dir = dir_parent+'script_results/qhdrfapm/'
  if not os.path.isdir(dir): os.makedirs(dir)
  with open(dir_parent+'script_results/qhdrfapm/output.csv', 'w') as output_dat:
    output_dat.write('variable,value\n')     
  
  #output conifuration settings
  #enable_single_csv_output = int(input_data[28])
  
  enable_optimization_csv_output = int(input_data[30])
  enable_show_iteration_number = int(input_data[44])
  enable_show_inputs = int(input_data[45])
  enable_current_density = int(input_data[46]) 
  enable_voltage_leeway = int(input_data[49])
  enable_eddy_losses_calc = int(input_data[26])
  enable_general_calc = int(input_data[25])
  
  ###########################
  # Read and Update iteration number #
  ###########################

  run_flags = np.genfromtxt(dir_parent+'script_results/main/running_flags.csv',skip_header=0,delimiter=',',usecols=(1),unpack=True)
  iteration_number = int(run_flags)
  
  ################
  # Machine Data #
  ################
  if input_method == READ_INPUT:
    
    p		= input_data[0]							#pole pairs
    kq		= input_data[1]							#coils per pole pair per phase                           [m]
    l		= input_data[5] 						#axial rotor/stator length                    [m]
    g		= input_data[6] 						#each airgap length
    hc		= input_data[7] 						#coil/winding thickness                       [m]
    kmo		= input_data[8] 						#radial outer PM width / pole pitch
    #kmi	= input_data[62] 						#radial inner PM width / pole pitch
    Brem	= input_data[9] 						#remnant flux density of NdBFe N48 PMs        [T]
    muR		= input_data[10] 						#recoil permeability of the PMs
  
    #f_e	= input_data[11]						#electrical excitation frequency	      [Hz]

    active_parts= int(input_data[19])
    turnsPerCoil= int(input_data[20])
    N = turnsPerCoil
    fill_factor = input_data[21]						#according to Gert's calculations, but we are not sure what value Maxwell actually used (2015/05/27)
    
    J		= input_data[31]*sqrt(2)					#input current density [A/mm^2] (RMS value is given by input file, thus multiply by sqrt(2)
    kc		= input_data[32]						#coil winding ratio range(0-1)
    hmo_r	= input_data[36]						#outer magnet width
    hmi_r	= input_data[37]						#inner magnet width
    ri		= input_data[38]						#most inner radius point on machine
    ro		= input_data[39]						#most outer radius point on machine

    
    go		= input_data[53]
    gi		= input_data[54]
    x_ri	= input_data[55]
    x_hmo_r	= input_data[56]
    #x_go	= input_data[57]
    x_hc	= input_data[58]
    #x_gi	= input_data[59]
    x_hmi_r	= input_data[60]
    n_rpm	= input_data[61]
    f_e		= (n_rpm*p*2)/120    
    omega_m=n_rpm*pi/30
    variable_no = int(input_data[52])
    perturb_count = np.zeros(int(variable_no))
    #x_hmo_rt	= input_data[63]
    #x_hmi_rt	= input_data[64]    
    
    peak_voltage_allowed = (input_data[33]*0.95*1.15)/sqrt(3)
    wire_resistivity     = input_data[18]
    coil_temperature	 = input_data[34]
    
    T=1/f_e
    t=0*T/12
    gamma=0
    
    #parallel circuits
    a=1
    
    if enable_show_inputs == 1:
      extra_input_info = np.genfromtxt(args[0],skip_header=1,delimiter=',',usecols=(2,3),unpack=True)
      ri_min  =  extra_input_info[1][38]
      hmo_r_min =  extra_input_info[1][36]
      go_min  =  extra_input_info[1][53]
      hc_min  =  extra_input_info[1][7]
      gi_min  =  extra_input_info[1][54]
      hmi_r_min =  extra_input_info[1][37]
    
    contour_figsize=(6,8)
    colorbar_shrink=0.25
    colorbar_aspect=10
    
    if optimise_interface != 1:
      if presentation:
	mpl.rcParams['font.family']='sans-serif'
	mpl.rcParams['font.sans-serif']='Helvetica'
	mpl.rcParams['text.latex.preamble']=r'\usepackage{helvet}',r'\usepackage{sansserifmath}',r'\renewcommand{\familydefault}{\sfdefault}'
	mpl.rcParams['axes.labelsize']=12
	mpl.rcParams['xtick.labelsize']=10
	mpl.rcParams['ytick.labelsize']=10
      
    #####################################
    # Calculation of Derived Quantities #
    #####################################
    km=kmo #TODO: kmi kmo
    kDelta=kc #TODO: is dit kc of (1-kc)? Ek is op die regte spoor, die definisie word gegee op Phd p.21
    #Brem=B_r
    
    #mechanical speed
    #omega_m=n*pi/30

    #coil info
    #q=kq*p
    #Q=3*q
    Q = int(kq * 3 * p)
    q = int(kq*p)
    Delta_max=pi/6
    Delta=kDelta*Delta_max      
     

    ##############################################
    # The number of harmonics for which to solve #
    ##############################################
    M=112*2 #from analytical_solution_i.py
  
    m_PM_range=np.arange(int(M/p))*2+1
    m_AR_range=np.arange(int(M/q)*2)+1
  
  elif input_method == MANUAL_INPUT:
    p=     14 #pole pairs
    kq=   1/2 #coils per pole pair per phase
    rn=120E-3 #nominal radius                               [m]
    hy= 25E-3 #dummy air cored yoke thickness               [m]
    hm=  6E-3 #magnet thickness                             [m]
    l=  40E-3 #axial rotor/stator length                    [m]
    g=   1E-3 #each airgap length
    h=   9E-3 #coil/winding thickness                       [m]

    km=   0.5 #radial PM width / pole pitch
    Brem= 1.4 #remnant flux density of NdBFe N48 PMs        [T]
    muR= 1.05 #recoil permeability of the PMs


  if enable_show_inputs == 1:
    extra_input_info = np.genfromtxt(args[0],skip_header=1,delimiter=',',usecols=(2,3),unpack=True)
    ri_min  =  extra_input_info[1][38]
    hmo_r_min =  extra_input_info[1][36]
    go_min  =  extra_input_info[1][53]
    hc_min  =  extra_input_info[1][7]
    gi_min  =  extra_input_info[1][54]
    hmi_r_min =  extra_input_info[1][37]

    ###DEBUGGING
    if enable_variable_test == True:
      if __name__ != '__main__':	#when this script is invoked by an other script
	print("hallo daar")
	#these variable values are decided by main.py
	g	= input_data[6] 						#each airgap length
	hc	= input_data[7] 						#magnet thickness   
	hmo_r	= input_data[36]						#outer magnet width
	hmi_r	= input_data[37]						#inner magnet width
	ri	= input_data[38]						#most inner radius point on machine
      else:				#when this script is executed by itself

	ri  = (ro - ri_min - hmo_r_min - go_min - hc_min - gi_min - hmi_r_min)*x_ri  + ri_min
	hmo_r = (ro - ri     - hmo_r_min - go_min - hc_min - gi_min - hmi_r_min)*x_hmo_r + hmo_r_min
	go  = (ro - ri     - hmo_r     - go_min - hc_min - gi_min - hmi_r_min)*x_go  + go_min
	hc  = (ro - ri     - hmo_r     - go     - hc_min - gi_min - hmi_r_min)*x_hc  + hc_min
	gi  = (ro - ri     - hmo_r     - go     - hc     - gi_min - hmi_r_min)*x_gi  + gi_min
	hmi_r = (ro - ri     - hmo_r     - go     - hc     - gi     - hmi_r_min)*x_hmi_r + hmi_r_min
	
	#print go
	
    ###

    #if enable_optimization_csv_output == 1 and iteration_number == 0:
	  #with open(dir_parent+'script_results/qhdrfapm/optimization_output.csv', 'w') as opt_output_file:
	    #opt_output_file.write('p,kq,l,g,hmi_r,hc,hmo_r,kmo,kc,ri,ro,number_of_layers_eddy,wire_diameter,parallel_stranded_conductors,turnsPerCoil,fill_factor,J,max_input_phase_current,max_induced_phase_voltage,max_input_phase_voltage,average_torque_dq,magnet_mass,copper_mass,Ra_analytical,efficiency,S,Q,P,P_out,P_conductive_analytical,P_eddy,P_wf,P_core_loss,Total_losses,power_factor,coil_side_area,max_Br_1,Br1,Br_THD*100\n')


  #############################################
  # Calculate remaining dimensions and ratios #
  #############################################
 
  #x_hmo_rt --> gives ratio between hight of outer radially magnetized PMs compared to outer tangentially magnetized PMs
  #hmo_t = x_hmo_rt*hmo_r
  #x_hmi_rt --> gives ratio between hight of inner radially magnetized PMs compared to inner tangentially magnetized PMs
  #hmi_t = x_hmi_rt*hmi_r
  
 
  #convert these parameters to my script's parameters
  magnetTotal = 4*p
  innerMagnetRadiusA = ri
  innerMagnetRadiusB = ri + hmi_r
  outerMagnetRadiusA = ri + hmi_r + gi + hc + go
  outerMagnetRadiusB = ro

  CoilRadiusA 	= ri + hmi_r + gi
  CoilRadiusB 	= ri + hmi_r + gi + hc

  
  innerBlockRadius = CoilRadiusA				#assuming the coil resin has no thickness
  outerBlockRadius = CoilRadiusB				#assuming the coil resin has no thickness
  coilBlockWidthRadial = outerBlockRadius - innerBlockRadius
  stack_length = l
  Br = Brem
  mur = muR
  coilsTotal = int(kq * 3 * p)


  magnetPitch = (2*pi)/magnetTotal
  axial_outer_PM_pitch = (1-kmo)*2*magnetPitch
  axial_inner_PM_pitch = (1-kmo)*2*magnetPitch
  #axial_inner_PM_pitch = (1-kmi)*2*magnetPitch
  radial_outer_PM_pitch = kmo*2*magnetPitch
  #radial_inner_PM_pitch = kmi*2*magnetPitch
  radial_inner_PM_pitch = kmo*2*magnetPitch
  angle_inc = 0.5*magnetPitch

  innerYoke = 0.9*ri
  outerYoke = 1.1*ro

  magnetPitch = (2*pi)/magnetTotal
  poles = int(magnetTotal/2)					#number of poles in rotor1, and number of poles in rotor2
  mechRotation = (4*pi)/poles					#only rotate one electrical full period (amount of radians the machine should mechanically rotate) ##mechRotation = 2*pi * (2/poles)
  rotor_symmetry_factor = int(4*magnetTotal/poles)
  symmetry_angle = (magnetTotal/(poles/4))*magnetPitch
  rotorDAxisOffset = 1.5*magnetPitch				#this angle is used to move rotor d-axis inline with the x-axis (assuming the transformation angle = 0 because we assume the phase-A axis also to be there)
  coilBlockPitch = (2*pi)/coilsTotal
  meanBlockRadius = innerBlockRadius + 0.5*coilBlockWidthRadial

  
  coil_side_radians = 0.5*kc*coilBlockPitch
  blockAngle = (1-kc)*coilBlockPitch
  coil_side_area = (CoilRadiusB**2 - CoilRadiusA**2)*(coil_side_radians/2)	#[m^2]
  #single_turn_area = parallel_stranded_conductors*pi*((wire_diameter/2)**2)

  if enable_current_density == True: 
    current_through_slot = J*(1000**2)*coil_side_area*fill_factor			#[A]
    area_available_per_turn = (coil_side_area*fill_factor*1000**2)/N
    N = 1 #turns per coil will be increased until voltage limit is reached
    iq = current_through_slot/N    
    Ip = iq
    max_input_phase_current = Ip
    if debug == 1:
      print("Using peak current density value of J = %f [A/mm2], which implies peak phase current Ip = %f [A]"%(J,Ip))
  else:
    Ip = 20
    if debug == 1:
      print("Using current value of Ip = %f [A]"%Ip)
  
  windingRadians = coil_side_radians
  
  N_L = 2							#Number of layer stack windings (if N_L == 1, then an error is given)
  N_S = 1							#Overlap number. This is a non-overlapping coil structure, thus N_S = 1
  phase = [1,-1,3,-3,2,-2]

  end_winding_radius = ((blockAngle+windingRadians)*meanBlockRadius)/2

  #Air-Gaps
  innerGap = innerBlockRadius - innerMagnetRadiusB
  outerGap = outerMagnetRadiusA - outerBlockRadius

  if enable_show_inputs == 1:
    pretty_input_table = PrettyTable(['SEMFEM Input Variable','Perturb Count','Minimum Value','Maximum Value','Initial Value','Present Value','Percentage Change','Unit'])
    pretty_input_table.align = 'l'
    pretty_input_table.border= True
    extra_input_info = np.genfromtxt(args[0],skip_header=1,delimiter=',',usecols=(2,3),unpack=True)

    if iteration_number == 0 or __name__ == '__main__':
      for i in range(0,np.size(perturb_count)):
	perturb_count[i] = 0
    else:
      perturb_count = np.genfromtxt(dir_parent+'script_results/main/perturb_count.csv',skip_header=1,usecols=(1),delimiter=",",unpack=True)
      #perturb_count = np.loadtxt('./script_results/main/perturb_count.csv',skip_header=1,usecols=(1),delimiter=",",unpack=True)
 
    pretty_input_table.add_row(["ABSOLUTE VALUES","","","","","","",""])
    pretty_input_table.add_row(["ro",0,extra_input_info[1][39],extra_input_info[0][39],input_data_original[39],ro,"%.3f"%(((ro-input_data_original[39])/input_data_original[39])*100),"[m]"])
    pretty_input_table.add_row(["ri",int(perturb_count[0]),extra_input_info[1][38],extra_input_info[0][38],input_data_original[38],ri,"%.3f"%(((ri-input_data_original[38])/input_data_original[38])*100),"[m]"])
    pretty_input_table.add_row(["hmo_r",int(perturb_count[1]),extra_input_info[1][36],extra_input_info[0][36],input_data_original[36],hmo_r,"%.3f"%(((hmo_r-input_data_original[36])/input_data_original[36])*100),"[m]"])
    pretty_input_table.add_row(["hc",int(perturb_count[2]),extra_input_info[1][7],extra_input_info[0][7],input_data_original[7],hc,"%.3f"%(((hc-input_data_original[7])/input_data_original[7])*100),"[m]"])
    pretty_input_table.add_row(["hmi_r",int(perturb_count[3]),extra_input_info[1][37],extra_input_info[0][37],input_data_original[37],hmi_r,"%.3f"%(((hmi_r-input_data_original[37])/input_data_original[37])*100),"[m]"])    
    pretty_input_table.add_row(["g",0,extra_input_info[1][6],extra_input_info[0][6],input_data_original[6],g,"%.3f"%(((g-input_data_original[6])/input_data_original[6])*100),"[m]"])    
    pretty_input_table.add_row(["l",0,extra_input_info[1][5],extra_input_info[0][5],input_data_original[5],l,"%.3f"%(((l-input_data_original[5])/input_data_original[5])*100),"[m]"])        
    pretty_input_table.add_row(["kmo",int(perturb_count[4]),extra_input_info[1][8],extra_input_info[0][8],input_data_original[8],kmo,"%.3f"%(((kmo-input_data_original[8])/input_data_original[8])*100),"[p.u.]"])
    pretty_input_table.add_row(["kc",int(perturb_count[5]),extra_input_info[1][32],extra_input_info[0][32],input_data_original[32],kc,"%.3f"%(((kc-input_data_original[32])/input_data_original[32])*100),"[p.u.]"])
    pretty_input_table.add_row(["p",int(perturb_count[6]),extra_input_info[1][0],extra_input_info[0][0],input_data_original[0],p,"%.3f"%(((p-input_data_original[0])/input_data_original[0])*100),"[p.u.]"])
    #pretty_input_table.add_row(["kmi",int(perturb_count[7]),extra_input_info[1][62],extra_input_info[0][62],input_data_original[62],kmi,"%.3f"%(((kmi-input_data_original[62])/input_data_original[62])*100),"[p.u.]"])     
    #pretty_input_table.add_row(["go",int(perturb_count[10]),extra_input_info[1][53],extra_input_info[0][53],input_data_original[53],go,"%.3f"%(((go-input_data_original[53])/input_data_original[53])*100),"[m]"])
    #pretty_input_table.add_row(["gi",int(perturb_count[11]),extra_input_info[1][54],extra_input_info[0][54],input_data_original[54],gi,"%.3f"%(((gi-input_data_original[54])/input_data_original[54])*100),"[m]"])
    
    pretty_input_table.add_row(["RELATIVE VALUES","","","","","","",""])
    pretty_input_table.add_row(["x_ri" ,int(perturb_count[0]),extra_input_info[1][55],extra_input_info[0][55],input_data_original[55],x_ri,"%.3f"%(((x_ri-input_data_original[55])/input_data_original[55])*100),"[p.u.]"])
    pretty_input_table.add_row(["x_hmo_r",int(perturb_count[1]),extra_input_info[1][56],extra_input_info[0][56],input_data_original[56],x_hmo_r,"%.3f"%(((x_hmo_r-input_data_original[56])/input_data_original[56])*100),"[p.u.]"])
    #pretty_input_table.add_row(["x_go" ,int(perturb_count[10]),extra_input_info[1][57],extra_input_info[0][57],input_data_original[57],x_go,"%.3f"%(((x_go-input_data_original[57])/input_data_original[57])*100),"[p.u.]"])
    pretty_input_table.add_row(["x_hc" ,int(perturb_count[2]),extra_input_info[1][58],extra_input_info[0][58],input_data_original[58],x_hc,"%.3f"%(((x_hc-input_data_original[58])/input_data_original[58])*100),"[p.u.]"])
    #pretty_input_table.add_row(["x_gi" ,int(perturb_count[11]),extra_input_info[1][59],extra_input_info[0][59],input_data_original[59],x_gi,"%.3f"%(((x_gi-input_data_original[59])/input_data_original[59])*100),"[p.u.]"])
    #pretty_input_table.add_row(["x_hmi_r" ,int(perturb_count[3]),extra_input_info[1][60],extra_input_info[0][60],input_data_original[60],x_hmi_r,"%.3f"%(((x_hmi_r-input_data_original[60])/input_data_original[60])*100),"[p.u.]"])
    #pretty_input_table.add_row(["x_hmo_rt",int(perturb_count[8]),extra_input_info[1][63],extra_input_info[0][63],input_data_original[63],x_hmo_rt,"%.3f"%(x_hmo_rt*100),"[p.u.]"]) 
    #pretty_input_table.add_row(["x_hmi_rt",int(perturb_count[9]),extra_input_info[1][64],extra_input_info[0][64],input_data_original[64],x_hmi_rt,"%.3f"%(x_hmi_rt*100),"[p.u.]"])
    
    save_stdout = sys.stdout
    sys.stdout = open(dir_parent+"pretty_input_table.txt", "wb")
    print pretty_input_table
    sys.stdout = save_stdout    

    if enable_terminal_output == 1:
      print pretty_input_table


  ###########
  # Regions #
  ###########

  I   = 0
  II  = 1
  III = 2
  IV  = 3
  V   = 4

  ############################################
  # Permeability's for the different Regions #
  ############################################

  mu0   =4*pi*1E-7
  murI  =1
  murII =muR
  murIII=1
  murIV =muR
  murV  =1

  ##################################################
  # Defining Boundaries for the Subdomain Analysis #
  ##################################################
  
  
  #all radii values will be relative to ensure that we work with reasonably sized values when computing the A-matrix.
  rn  = innerMagnetRadiusB + 0.5*(outerMagnetRadiusA - innerMagnetRadiusB) #NB
  rii = ri/rn
  riii= innerMagnetRadiusB/rn
  riv = outerMagnetRadiusA/rn
  rv  = outerMagnetRadiusB/rn
  rvi = outerYoke/rn
  
  #from now on, ri will obtain a different definition, namely ri = innerYokeRadiusA/rn
  #previously, ri was ri = innerMagnetRadiusA
  
  ri  = innerYoke/rn
  
  #inner & outer magnet centres
  rcim=	innerMagnetRadiusA + 0.5*(innerMagnetRadiusB-innerMagnetRadiusA)
  rcom= outerMagnetRadiusA + 0.5*(outerMagnetRadiusB-outerMagnetRadiusA)
  
  
  #all values starting with "h" will stay in ABSOLUTE values
  hy_o = outerYoke - outerMagnetRadiusB #outer yoke thickness
  hy_i = innerMagnetRadiusA - innerYoke #inner yoke thickness
  
  
  '''
  ri  =(rn-h/2-g-hm-hy)/rn
  rii =(rn-h/2-g-hm)/rn
  riii=(rn-h/2-g)/rn
  riv =(rn+h/2+g)/rn
  rv  =(rn+h/2+g+hm)/rn
  rvi =(rn+h/2+g+hm+hy)/rn
  '''
  
  ##############################################
  # The number of harmonics for which to solve #
  ##############################################

  Mmax=19
  #Mmax=37
  
  ###########################
  # Other derived quatities #
  ###########################

  beta=(1-km)/2*pi/p                    #beta angle used for Fourier series expansion #NB
  q=kq*p                                 #coils per phase
  #m_range=np.arange(int((Mmax+2)/2))*2+1 #the range of harmonics to solve for
  #print("m_range")
  #print(m_range)
  #print("m_range = %d"%m_range)


  #modfified
  #m_range=np.arange(int((Mmax+2)/2))*2+1 #the range of harmonics to solve for
  #m_range=np.arange(int((Mmax+2)/2))*2+1 #the range of harmonics to solve for


  ##################################################
  # The range of r & phi values for which to solve #
  ##################################################

  dr=1E-3
  r_range=np.arange(ri,rvi+dr,dr) #notice that r_range is RELATIVE values
  #print r_range


  M = 128 #In order to utilise the FFT optimally later on
  phi_range=np.linspace(-pi/q,pi/q,int(M/kq))


  ############################
  # Winding Factor Functions #
  ############################
  def kw_pitch_def(m):
    r'''The Winding Pitch Factor'''
    if Type=='O':
      return np.sin(m*pi/2)
    elif Type=='I':
      return np.sin(m*pi/6)
    elif Type=='II':
      return np.sin(m*(pi/3-Delta))
    else:
      raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."

  def kw_slot_def(m):
    r'''The Winding Slot Factor'''
    return np.sin(m*Delta)/(m*Delta)

  def kT_def(m):
    r'''The torque constant of the machine'''
    return 3*q*rn*l*N/a*kw_pitch_def(m)*kw_slot_def(m)*Br1_PM

  ################################################################################
  # A lookup function to determine the index value of a specific floating point  #
  # value in a floating point array                                              #
  ################################################################################

  def lookup(ref,array,tol):
    r'''A lookup function to determine the index value of a specific floating point value in a floating point array,
  due to the fact that a floating point value of say "0.232" is represented in python as "0.23200000000000001"'''
    for i,val in enumerate(array):
      if abs((val-ref))<tol:
	return i

  #################################
  # General Function Definitions  #
  #################################

  # NOT USED #
  def am_dM0r_def(m):
    return 4*p*Brem/(pi*mu0)*cos(m*p*beta)

  # NOT USED #
  def dM0r_def():
    tmp=0.0
    for m in (np.arange(M)*2+1):
      tmp+=am_dM0r_def(m)*np.cos(m*p*phi_range)
    return tmp

  def bm_Jz_def(Ip,m): #from analytical_solution_i.py
    r'''The "bm" coefficient for the Fourier series expansion of the Current Density Distribution expressed in terms of the Conductor Density Distribution'''
    return -(3*q*Ip*N)/(rn*hc*pi)*kw_pitch_def(m)*kw_slot_def(m) #TODO: rn or rn/rn

  def Jz_def(Ip,t): #from analytical_solution_i.py
    r'''The Current Density Distribution expressed in terms of the Conductor Density Distribution'''
    tmp=0.0
    for m in (3*m_AR_range-2):
    #for m in (3*m_range-2):
      tmp+=bm_Jz_def(Ip,m)*np.sin(m*q*phi_range-p*omega_m*t) if Type=='O' else bm_Jz_def(Ip,m)*np.sin(m*q*phi_range+p*omega_m*t)
    for m in (3*m_AR_range-1):
    #for m in (3*m_range-1):
      tmp+=bm_Jz_def(Ip,m)*np.sin(m*q*phi_range+p*omega_m*t) if Type=='O' else bm_Jz_def(Ip,m)*np.sin(m*q*phi_range-p*omega_m*t)
    return tmp

  #########################
  # The Forcing Functions #
  #########################

  def GmII_def(m):
    r'''The Forcing function due to the Permanent Magnets'''
    return 4*Brem*(m*p*cos(m*p*beta)-sin(m*p*beta))/(pi*m*(1-(m*p)**2))

  def GmIV_def(m):
    r'''The Forcing function due to the Permanent Magnets'''
    return 4*Brem*(m*p*cos(m*p*beta)+sin(m*p*beta))/(pi*m*(1-(m*p)**2))

  def UmII_def(m):
    r'''The Forcing function due to the Permanent Magnets'''
    return 4*Brem*(cos(m*p*beta)-m*p*sin(m*p*beta))/(pi*m*(1-(m*p)**2))

  def UmIV_def(m):
    r'''The Forcing function due to the Permanent Magnets'''
    return 4*Brem*(cos(m*p*beta)+m*p*sin(m*p*beta))/(pi*m*(1-(m*p)**2))

  def Gmr_AR_def(m,r):
    r'''The Forcing function due to the Armature Windings'''
    if m % 3 == 0:
      return 0
    else:
      return 3*mu0*Ip*N*r**2/(pi*rn*hc*q*m**2)*kw_pitch_def(m)*kw_slot_def(m)

  def dGmrdr_AR_def(m,r):
    r'''The derivative with respect to "r" of the Forcing function due to the Armature Windings'''
    if m % 3 == 0:
      return 0
    else:
      return 6*mu0*Ip*N*r/(pi*rn*hc*q*m**2)*kw_pitch_def(m)*kw_slot_def(m)

  ##################################
  # The Vector Potential Functions #
  ##################################

  def am_Az_PM_def(i,m,r,Region):
    r'''The "am" coefficient for the Fourier series expansion of the Magnetic Vector Potential
  due to the Armature Windings'''
    if Region==II:
      return Cm_PM[Region][i]*r**(m*p)+Dm_PM[Region][i]*r**(-m*p)+GmII_def(m)*r
    elif Region==IV:
      return Cm_PM[Region][i]*r**(m*p)+Dm_PM[Region][i]*r**(-m*p)+GmIV_def(m)*r
    else:
      #print Region
      #print i
      return Cm_PM[Region][i]*r**(m*p)+Dm_PM[Region][i]*r**(-m*p)
    return tmp

  def Az_PM_def(r,t,Region):
    r'''The Fourier series expansion of the Magnetic Vector Potential for a specific value of "r"
  due to the Permanent Magnets'''
    tmp=0.0
    for i, m in enumerate(m_PM_range):
    #for i, m in enumerate(m_range):
      #tmp+=am_Az_PM_def(i,m,r,Region)*np.cos(m*p*phi_range)
      tmp+=am_Az_PM_def(i,m,r,Region)*np.cos(m*p*phi_range-m*p*omega_m*t-m*gamma)
    return tmp

  def bm_Az_AR_def(i,m,r,Region):
    r'''The "bm" coefficient for the Fourier series expansion of the Magnetic Vector Potential
  due to the Armature Windings'''
    if Region==III:
      return Cm_AR[Region][i]*r**(m*q)+Dm_AR[Region][i]*r**(-m*q)+Gmr_AR_def(m,r)
    else:
      return Cm_AR[Region][i]*r**(m*q)+Dm_AR[Region][i]*r**(-m*q)
    return tmp

  def Az_AR_def(r,t,Region):
    r'''The Fourier series expansion of the Magnetic Vector Potential for a specific value of "r"
  due to the Armature Windings'''
    tmp=0.0
    for i, m in enumerate(m_AR_range):
    #for i, m in enumerate(m_range):
      if m in (3*m_AR_range-2):
      #if m in (3*m_range-2):
	tmp+=bm_Az_AR_def(i,m,r,Region)*np.sin(m*q*phi_range-p*omega_m*t)
      elif m in (3*m_AR_range-1):
      #elif m in (3*m_range-1):
	tmp+=bm_Az_AR_def(i,m,r,Region)*np.sin(m*q*phi_range+p*omega_m*t)
    return tmp

  #####################################
  # The Radial Flux Density Functions #
  #####################################

  def bm_Br_PM_def(i,m,r,Region):
    r'''The "bm" coefficient for the Fourier series expansion of the Radial Flux Density
  due to the Permanent Magnets'''
    if Region==II:
      return -m*p*(Cm_PM[Region][i]*r**(m*p-1)+Dm_PM[Region][i]*r**(-m*p-1)+GmII_def(m))
    elif Region==IV:
      return -m*p*(Cm_PM[Region][i]*r**(m*p-1)+Dm_PM[Region][i]*r**(-m*p-1)+GmIV_def(m))
    else:
      return -m*p*(Cm_PM[Region][i]*r**(m*p-1)+Dm_PM[Region][i]*r**(-m*p-1))

  def int_bm_Br_III_PM_def(i,m,r):
    r'''The integral of r**2*"bm" (see above) in order to work out the torque with the Lorentz method'''
    return -m*p*(Cm_PM[III][i]/(m*p+2)*r**(m*p+2)+Dm_PM[III][i]/(-m*p+2)*r**(-m*p+2))

  def Br_PM_def(r,t,Region):
    r'''The Fourier series expansion of the Radial Flux Density for a specific value of "r"
  due to the Permanent Magnets'''
    tmp=0.0
    for i, m in enumerate(m_PM_range):
    #for i, m in enumerate(m_range):
      #tmp+=bm_Br_PM_def(i,m,r,Region)*np.sin(m*p*phi_range)
      tmp+=bm_Br_PM_def(i,m,r,Region)*np.sin(m*p*phi_range-m*p*omega_m*t-m*gamma)
    return tmp

  def am_Br_AR_def(i,m,r,Region):
    r'''The "am" coefficient for the Fourier series expansion of the Radial Flux Density
  due to the Armature Windings'''
    if Region==III:
	return -m*q/r*(Cm_AR[Region][i]*r**(m*q)+Dm_AR[Region][i]*r**(-m*q)+Gmr_AR_def(m,r))
    else:
	return -m*q/r*(Cm_AR[Region][i]*r**(m*q)+Dm_AR[Region][i]*r**(-m*q))

  def Br_AR_def(r,t,Region):
    r'''The Fourier series expansion of the Radial Flux Density for a specific value of "r"
  due to the Armature Windings'''
    tmp=0.0
    for i, m in enumerate(m_AR_range):
    #for i, m in enumerate(m_range):
      if m in (3*m_AR_range-2):
      #if m in (3*m_range-2):
	tmp+=am_Br_AR_def(i,m,r,Region)*np.cos(m*q*phi_range-p*omega_m*t) if Type=='O' else am_Br_AR_def(i,m,r,Region)*np.cos(m*q*phi_range+p*omega_m*t)
      elif m in (3*m_AR_range-1):
      #elif m in (3*m_range-1):
	tmp+=am_Br_AR_def(i,m,r,Region)*np.cos(m*q*phi_range+p*omega_m*t) if Type=='O' else am_Br_AR_def(i,m,r,Region)*np.cos(m*q*phi_range-p*omega_m*t)
    return tmp

  ########################################
  # The Azimuthal Flux Density Functions #
  ########################################

  def am_Bt_PM_def(i,m,r,Region):
    r'''The "am" coefficient for the Fourier series expansion of the Azimuthal Flux Density
  due to the Permanent Magnets'''
    if Region==II:
      return -(m*p*Cm_PM[Region][i]*r**(m*p-1)-m*p*Dm_PM[Region][i]*r**(-m*p-1)+GmII_def(m))
    elif Region==IV:
      return -(m*p*Cm_PM[Region][i]*r**(m*p-1)-m*p*Dm_PM[Region][i]*r**(-m*p-1)+GmIV_def(m))
    else:
      return -m*p*(Cm_PM[Region][i]*r**(m*p-1)-Dm_PM[Region][i]*r**(-m*p-1))

  def Bt_PM_def(r,t,Region):
    r'''The Fourier series expansion of the Azimuthal Flux Density for a specific value of "r"
  due to the Permanent Magnets'''
    tmp=0.0
    for i, m in enumerate(m_PM_range):
    #for i, m in enumerate(m_range):
      #tmp+=am_Bt_PM_def(i,m,r,Region)*np.cos(m*p*phi_range)
      tmp+=am_Bt_PM_def(i,m,r,Region)*np.cos(m*p*phi_range-m*p*omega_m*t-m*gamma)
    return tmp

  def bm_Bt_AR_def(i,m,r,Region):
    r'''The "bm" coefficient for the Fourier series expansion of the Azimuthal Flux Density
  due to the Armature Windings'''
    if Region==III:
      return -m*q*(Cm_AR[Region][i]*r**(m*q-1)-Dm_AR[Region][i]*r**(-m*q-1)+dGmrdr_AR_def(m,r)/(m*q))
    else:
      return -m*q*(Cm_AR[Region][i]*r**(m*q-1)-Dm_AR[Region][i]*r**(-m*q-1))

  def Bt_AR_def(r,t,Region):
    r'''The Fourier series expansion of the Azimuthal Flux Density for a specific value of "r"
  due to the Armature Windings'''
    tmp=0.0
    for i, m in enumerate(m_AR_range):
    #for i, m in enumerate(m_range):
      if m in (3*m_AR_range-2):
      #if m in (3*m_range-2):
	tmp+=bm_Bt_AR_def(i,m,r,Region)*np.sin(m*q*phi_range-p*omega_m*t) if Type=='O' else bm_Bt_AR_def(i,m,r,Region)*np.sin(m*q*phi_range+p*omega_m*t)
      elif m in (3*m_AR_range-1):
      #elif m in (3*m_range-1):
	tmp+=bm_Bt_AR_def(i,m,r,Region)*np.sin(m*q*phi_range+p*omega_m*t) if Type=='O' else bm_Bt_AR_def(i,m,r,Region)*np.sin(m*q*phi_range-p*omega_m*t)
    return tmp

  ###########################################################################
  #The Flux-linkage at a specific value of "r" due to the Armature Windings #
  ###########################################################################

  def lambda_PM_def(r):
    r'''The Flux-linkage for a specific value of "r" due to the Permanent Magnets'''
    tmp=0
    for i, m in enumerate(m_PM_range):
    #for i, m in enumerate(m_range):
      tmp+=(-2*q*N*l/a)*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*am_Az_PM_def(i,m,r,Region=III)
    return tmp

  def lambda_AR_def(r):
    r'''The Flux-linkage for a specific value of "r" due to the Armature Windings'''
    tmp=0
    for i, m in enumerate(m_AR_range):
    #for i, m in enumerate(m_range):
      tmp+=(2*q*N*l/a)*kw_pitch_def(m)*kw_slot_def(m)*bm_Az_AR_def(i,m,r,Region=III)
      #FIXME: must be a "(-2*q*N*l/a)..." above
    return tmp

  def k_lambda():
    if Type=='O':
      return np.sin(Delta)/(Delta)
    elif Type=='I':
      return np.sin(pi/3)*np.sin(Delta)/(Delta)
    elif Type=='II':
      return np.sin((pi/Q-Delta)*p)*np.sin(Delta)/(Delta)
    else:
      raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."

  if debug == 1:
    print('The analysis will be done for a Type %s Machine' % (Type))


##################################################################################################################
######################################
###ROTOR (PERMANENT MAGNETS)###
######################################
##################################################################################################################

  #######################################
  # Initialise the PMAG solution arrays #
  #######################################

  CmI_PM  =np.array([])
  DmI_PM  =np.array([])
  CmII_PM =np.array([])
  DmII_PM =np.array([])
  CmIII_PM=np.array([])
  DmIII_PM=np.array([])
  CmIV_PM =np.array([])
  DmIV_PM =np.array([])
  CmV_PM  =np.array([])
  DmV_PM  =np.array([])

  ########################################################
  # Performing the PMAG Linear Solve for each value of m #
  ########################################################
  #print "m_PM_range"
  #print m_PM_range
  #for m in m_PM_range:
  for m in m_PM_range:
    #print ri**(-m*p)
    A=np.mat([[ri**(m*p)         ,  ri**(-m*p)         ,  0                   ,  0                    ,  0                  ,  0                   ,  0                  ,  0                   ,  0                ,  0                 ],
	      [rii**(m*p-1)      ,  rii**(-m*p-1)      , -rii**(m*p-1)        , -rii**(-m*p-1)        ,  0                  ,  0                   ,  0                  ,  0                   ,  0                ,  0                 ],
	      [murII*rii**(m*p-1), -murII*rii**(-m*p-1), -murI*rii**(m*p-1)   ,  murI*rii**(-m*p-1)   ,  0                  ,  0                   ,  0                  ,  0                   ,  0                ,  0                 ],
	      [0                 ,  0                  ,  riii**(m*p-1)       ,  riii**(-m*p-1)       , -riii**(m*p-1)      , -riii**(-m*p-1)      ,  0                  ,  0                   ,  0                ,  0                 ],
	      [0                 ,  0                  ,  murIII*riii**(m*p-1), -murIII*riii**(-m*p-1), -murII*riii**(m*p-1),  murII*riii**(-m*p-1),  0                  ,  0                   ,  0                ,  0                 ],
	      [0                 ,  0                  ,  0                   ,  0                    ,  riv**(m*p-1)       ,  riv**(-m*p-1)       , -riv**(m*p-1)       , -riv**(-m*p-1)       ,  0                ,  0                 ],
	      [0                 ,  0                  ,  0                   ,  0                    ,  murIV*riv**(m*p-1) , -murIV*riv**(-m*p-1) , -murIII*riv**(m*p-1),  murIII*riv**(-m*p-1),  0                ,  0                 ],
	      [0                 ,  0                  ,  0                   ,  0                    ,  0                  ,  0                   ,  rv**(m*p-1)        ,  rv**(-m*p-1)        , -rv**(m*p-1)      , -rv**(-m*p-1)      ],
	      [0                 ,  0                  ,  0                   ,  0                    ,  0                  ,  0                   ,  murV*rv**(m*p-1)   , -murV*rv**(-m*p-1)   , -murIV*rv**(m*p-1),  murIV*rv**(-m*p-1)],
	      [0                 ,  0                  ,  0                   ,  0                    ,  0                  ,  0                   ,  0                  ,  0                   ,  rvi**(m*p)       ,  rvi**(-m*p)       ]])

    B=np.mat([[ 0 ],
	      [ GmII_def(m)*rii  ],
	      [ murI*UmII_def(m)  ],
	      [-GmII_def(m)*riii ],
	      [-murIII*UmII_def(m)],
	      [ GmIV_def(m)*riv  ],
	      [ murIII*UmIV_def(m)],
	      [-GmIV_def(m)*rv   ],
	      [-murV*UmIV_def(m)  ],
	      [ 0 ]])

    X=np.linalg.solve(A,B)

    CmI_PM  =np.append(CmI_PM,  X[0,0])
    DmI_PM  =np.append(DmI_PM,  X[1,0])
    CmII_PM =np.append(CmII_PM, X[2,0])
    DmII_PM =np.append(DmII_PM, X[3,0])
    CmIII_PM=np.append(CmIII_PM,X[4,0])
    DmIII_PM=np.append(DmIII_PM,X[5,0])
    CmIV_PM =np.append(CmIV_PM, X[6,0])
    DmIV_PM =np.append(DmIV_PM, X[7,0])
    CmV_PM  =np.append(CmV_PM,  X[8,0])
    DmV_PM  =np.append(DmV_PM,  X[9,0])

    #dM0+=(4*p/pi)*np.cos(m*p*beta)*np.cos(m*p*phi_range)

  ###########################################################
  # Repack the PMAG Solution Coefficient into Matrix format #
  ###########################################################

  Cm_PM=np.array([CmI_PM,CmII_PM,CmIII_PM,CmIV_PM,CmV_PM])
  Dm_PM=np.array([DmI_PM,DmII_PM,DmIII_PM,DmIV_PM,DmV_PM])

  #print(Cm)
  #print(Dm)

  ###################################
  # Initialise PMAG Solution Arrays #
  ###################################

  Az_PM=np.zeros((r_range.size,phi_range.size))
  Br_PM=np.zeros((r_range.size,phi_range.size))
  Bt_PM=np.zeros((r_range.size,phi_range.size))
  Bmag_PM=np.zeros((r_range.size,phi_range.size))

  ##make PM arrays link to old name convention of analytical_solution_i.py
  #Az_PM = Az
  #Br_PM = Br
  #Bt_PM = Bt
  #Bmag_PM = Bmag

  #########################################################################
  # Calculate the PMAG Solution Arrays for each value of 'r' in 'r_range' #
  #########################################################################
  if optimise_interface != 1:
    for i,r in enumerate(r_range):
      if r<rii:
	Region=I
      elif r<riii:
	Region=II
      elif r<riv:
	Region=III
      elif r<rv:
	Region=IV
      else:
	Region=V
	
      #print np.shape(Cm)  
      Az_PM[i]=Az_PM_def(r,0,Region)
      Br_PM[i]=Br_PM_def(r,0,Region)
      Bt_PM[i]=Bt_PM_def(r,0,Region)
      Bmag_PM[i]=np.sqrt(Br_PM[i]**2+Bt_PM[i]**2) #magnitude of both radial and tangetial components
      #print("i=%f,  r=%f"%(i,r))
    
    #print np.shape(Az)

    ###############################################
    # Calculate Br values at specific values of r #
    ###############################################

    #Get index for the following radii
    i_rn=lookup(rn/rn,r_range,dr/2)
    i_rnmh2=lookup((rn-hc/2)/rn,r_range,dr/2)
    i_rnph2=lookup((rn+hc/2)/rn,r_range,dr/2)
    
    i_rcim=lookup(rcim/rn,r_range,0.1/1000)
    i_rcom=lookup(rcom/rn,r_range,0.1/1000)
    i_rciy=lookup((rn-hc/2-g-hmi_r-hy_i/2)/rn,r_range,0.1/1000)
    i_rcoy=lookup((rn+hc/2+g+hmo_r+hy_o/2)/rn,r_range,0.1/1000)

    Brmax_PM=Br_PM[i_rn].max()
    Brh_PM=np.abs(np.fft.fft(Br_PM[i_rn],M)*2/M)
    Bth_PM=np.abs(np.fft.fft(Bt_PM[i_rn],M)*2/M)
    
    Br1_PM=Brh_PM[1]
    Bt1_PM=Bth_PM[1]

    Br1_PM_range=np.array([])
    for r in np.linspace((rn-hc/2)/rn,(rn+hc/2)/rn,20):
      i=lookup(r,r_range,dr/2)  
      Br1_PM_range=np.append(Br1_PM_range,np.abs(np.fft.fft(Br_PM[i],M)*2/M)[1])

    Bt1_PM_range=np.array([])
    for r in np.linspace((rn-hc/2)/rn,(rn+hc/2)/rn,20):
      i=lookup(r,r_range,dr/2)  
      Bt1_PM_range=np.append(Bt1_PM_range,np.abs(np.fft.fft(Bt_PM[i],M)*2/M)[1])

    Br_PM_THD=np.sqrt(np.sum(Brh_PM[2:int((M)/2)]**2))/Br1_PM
    Bt_THD_PM=np.sqrt(np.sum(Bth_PM[2:int((M)/2)]**2))/Bt1_PM


##################################################################################################################
######################################
###STATOR COILS (ARMATURE REACTION)###
######################################
##################################################################################################################
  if optimise_interface != 1:  
    ###################################################
    # Redefining Boundaries for the Armature Reaction #
    ###################################################

    riii=(rn-hc/2)/rn #thus riii now starts after the inner airgap (where the inner radius of stator coils actually are)
    riv =(rn+hc/2)/rn #thus riv now starts before the outer airgap (where the outer radius of stator coils actually are)

    ##################################
    # Initialise the solution arrays #
    ##################################

    CmI_AR  =np.array([])
    DmI_AR  =np.array([])
    CmII_AR =np.array([])
    DmII_AR =np.array([])
    CmIII_AR=np.array([])
    DmIII_AR=np.array([])
    CmIV_AR =np.array([])
    DmIV_AR =np.array([])
    CmV_AR  =np.array([])
    DmV_AR  =np.array([])

    #########################################################################
    # Performing the Linear Solve for each value of m due to the Armature Windings Regions #
    #########################################################################

    #print "m_AR_range"
    #print m_AR_range

    #for m in m_AR_range:
      #print m

    #print riii
    #print m
    #print q
    #print m*q
    #print riii**(m*q)

    #for m in m_AR_range:
    for m in m_AR_range:
      A=np.array([[ri**(m*q)          ,  ri**(-m*q)        ,  0                  ,  0                   ,  0                 ,  0                  ,  0                 ,  0                  ,  0               ,  0                ],
		  [rii**(m*q)         ,  rii**(-m*q)       , -rii**(m*q)         , -rii**(-m*q)         ,  0                 ,  0                  ,  0                 ,  0                  ,  0               ,  0                ],
		  [murII*rii**(m*q-1)  , -murII*rii**(-m*q-1), -murI*rii**(m*q-1)   ,  murI*rii**(-m*q-1)   ,  0                 ,  0                  ,  0                 ,  0                  ,  0               ,  0                ],
		  [0                  ,  0                 ,  riii**(m*q)        ,  riii**(-m*q)        , -riii**(m*q)       , -riii**(-m*q)       ,  0                 ,  0                  ,  0               ,  0                ],
		  [0                  ,  0                 ,  murIII*riii**(m*q-1), -murIII*riii**(-m*q-1), -murII*riii**(m*q-1),  murII*riii**(-m*q-1),  0                 ,  0                  ,  0               ,  0                ],
		  [0                  ,  0                 ,  0                  ,  0                   ,  riv**(m*q)        ,  riv**(-m*q)        , -riv**(m*q)        , -riv**(-m*q)        ,  0               ,  0                ],
		  [0                  ,  0                 ,  0                  ,  0                   ,  murIV*riv**(m*q-1) , -murIV*riv**(-m*q-1) , -murIII*riv**(m*q-1),  murIII*riv**(-m*q-1),  0               ,  0                ],
		  [0                  ,  0                 ,  0                  ,  0                   ,  0                 ,  0                  ,  rv**(m*q)         ,  rv**(-m*q)         , -rv**(m*q)       , -rv**(-m*q)       ],
		  [0                  ,  0                 ,  0                  ,  0                   ,  0                 ,  0                  ,  murV*rv**(m*q-1)   , -murV*rv**(-m*q-1)   , -murIV*rv**(m*q-1),  murIV*rv**(-m*q-1)],
		  [0                  ,  0                 ,  0                  ,  0                   ,  0                 ,  0                  ,  0                 ,  0                  ,  rvi**(m*q)      ,  rvi**(-m*q)      ]])

      B=np.array([[ 0 ],
		  [ 0 ],
		  [ 0 ],
		  [ Gmr_AR_def(m,riii)],
		  [ murII*dGmrdr_AR_def(m,riii)/(m*q)],
		  [-Gmr_AR_def(m,riv)],
		  [-murIV*dGmrdr_AR_def(m,riv)/(m*q)],
		  [ 0 ],
		  [ 0 ],
		  [ 0 ]])

      X=np.linalg.solve(A,B)

      CmI_AR  =np.append(CmI_AR,  X[0,0])
      DmI_AR  =np.append(DmI_AR,  X[1,0])
      CmII_AR =np.append(CmII_AR, X[2,0])
      DmII_AR =np.append(DmII_AR, X[3,0])
      CmIII_AR=np.append(CmIII_AR,X[4,0])
      DmIII_AR=np.append(DmIII_AR,X[5,0])
      CmIV_AR =np.append(CmIV_AR, X[6,0])
      DmIV_AR =np.append(DmIV_AR, X[7,0])
      CmV_AR  =np.append(CmV_AR,  X[8,0])
      DmV_AR  =np.append(DmV_AR,  X[9,0])

    #############################################################
    # Repack the Stator Coefficient Solution into Matrix format #
    #############################################################

    Cm_AR=np.array([CmI_AR,CmII_AR,CmIII_AR,CmIV_AR,CmV_AR])
    Dm_AR=np.array([DmI_AR,DmII_AR,DmIII_AR,DmIV_AR,DmV_AR])

    #####################################
    # Initialise Stator Solution Arrays #
    #####################################
    Az_AR=np.zeros((r_range.size,phi_range.size))
    Br_AR=np.zeros((r_range.size,phi_range.size))
    Bt_AR=np.zeros((r_range.size,phi_range.size))
    Bmag_AR=np.zeros((r_range.size,phi_range.size))

  ########################################################################################
  # Calculate a solution for each value of 'r' in 'r_range' due to the Armature Windings #
  ########################################################################################

  if optimise_interface != 1:
    for i,r in enumerate(r_range):
      if r<rii:
	Region=I
      elif r<riii:
	Region=II
      elif r<riv:
	Region=III
      elif r<rv:
	Region=IV
      else:
	Region=V
      Az_AR[i]=Az_AR_def(r,t,Region)
      Br_AR[i]=Br_AR_def(r,t,Region)
      Bt_AR[i]=Bt_AR_def(r,t,Region)
      Bmag_AR[i]=np.sqrt(Br_AR[i]**2+Bt_AR[i]**2) #magnitude of both radial and tangetial components

  if optimise_interface != 1:
    Brh_AR=np.abs(np.fft.fft(Br_AR[i_rn],M)*2/M)
    Bth_AR=np.abs(np.fft.fft(Bt_AR[i_rn],M)*2/M)

    Br1_AR=Brh_AR[1]
    Bt1_AR=Bth_AR[1]

    Az_total=Az_PM+Az_AR
    Br_total=Br_PM+Br_AR
    Bt_total=Bt_PM+Bt_AR
    Bmag_total=Bmag_PM+Bmag_AR
    
    ###
    Brmax_AR=Br_AR[i_rn].max()
    
    Br1_AR_range=np.array([])
    for r in np.linspace((rn-hc/2)/rn,(rn+hc/2)/rn,20):
      i=lookup(r,r_range,dr/2)  
      Br1_AR_range=np.append(Br1_AR_range,np.abs(np.fft.fft(Br_AR[i],M)*2/M)[1])

    Bt1_AR_range=np.array([])
    for r in np.linspace((rn-hc/2)/rn,(rn+hc/2)/rn,20):
      i=lookup(r,r_range,dr/2)  
      Bt1_AR_range=np.append(Bt1_AR_range,np.abs(np.fft.fft(Bt_AR[i],M)*2/M)[1])
    
    Br_AR_THD=np.sqrt(np.sum(Brh_AR[2:int((M)/2)]**2))/Br1_AR
    Bt_AR_THD=np.sqrt(np.sum(Bth_AR[2:int((M)/2)]**2))/Bt1_AR

  if debug == 1:
    print('Brmax_AR|rn   = %g T' % Brmax_AR)			#all harmonics included peak value @ rn
    print('Br1_AR|rn     = %g T' % Br1_AR)			#1st harmonic peak value @ rn
    print('Br1_AR|ave    = %g T' % np.average(Br1_AR_range))	#average of 1st harmonic peak values from rn-hc/2 to rn+hc/2
    print('THD(Br_AR|rn) = %g %%' % (Br_AR_THD*100,))
  ###
  
  t_range=np.linspace(0,T,181)
  #print t_range


  lambda_PM=lambda_PM_def(rn/rn) #TODO: should it be rn, or rn/rn?
  if optimise_interface != 1:
    lambda_AR=lambda_AR_def(rn/rn)

  #scale everything back to [m] values
  lambda_PM = lambda_PM*rn
  if optimise_interface != 1:
    lambda_AR = lambda_AR*rn

  #m=1 if Type=='O' else 2

  if optimise_interface != 1:
    lambda_PM_approx=2*(rn)*l*N*Br1_PM/a*kq*kw_pitch_def(1/kq)*kw_slot_def(1/kq) #TODO: should it be rn, or rn/rn?
    lambda_AR_approx=(2*q*N*l/a)*kw_pitch_def(1)*kw_slot_def(1)*bm_Az_AR_def(0,1,rn/rn,Region=III) #TODO: should it be rn, or rn/rn?

  EMK_PM=lambda_PM*omega_m*p
  if optimise_interface != 1:
    EMK_AR=lambda_AR*2*pi*f_e

  if optimise_interface != 1:
    EMK_PM_approx=lambda_PM_approx*omega_m*p

  #now that we have the induced voltage on the windings, we can determine the maximum number of allowed turns
  N = int(peak_voltage_allowed/EMK_PM)
  max_induced_phase_voltage = N*EMK_PM
  max_input_phase_current = max_input_phase_current/N
  wire_resistivity = wire_resistivity*(1+0.0039*(coil_temperature-20)) 
  if debug == 1:
    print("\n(0) Wire_resistivity = %f [Ohm.m]  @ T = %d [Celsius Deg]"%(wire_resistivity,coil_temperature))
    print("(0) Maximum allowed peak voltage for this design V_a(ln) = %f [V]"%peak_voltage_allowed)
    print("(0) Peak voltage induced in 1 turn V_a(ln) = %f [V/t]"%EMK_PM)
    print("\n(1) Maximum number of turns allowed N = %d turns"%N)
    print("(1) Peak voltage induced by all turns V_a(ln) = %f [V]"%max_induced_phase_voltage)
    print("(1) Peak input phase current Ia = %f [A]"%max_input_phase_current)

  #CALCULATE I^2R CONDUCTION LOSSES (ANALYTICAL)

  
  l_coil = 2*(l+(pi*end_winding_radius))*N
  l_total = (coilsTotal/3)*l_coil
  Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)
  max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical
  if debug == 1:
    print("(1) Ra_analytical = %f [Ohm]"%Ra_analytical)
    print("(1) Peak system input phase voltage V_a(ln) = %f [V]"%max_input_phase_voltage)
  
  
  #now we check that this max_input_phase_voltage also falls below the maximum allowed system voltage
  while max_input_phase_voltage > peak_voltage_allowed:
    N = N - 1
    
    area_available_per_turn = (coil_side_area*fill_factor*1000**2)/N
    max_induced_phase_voltage = N*EMK_PM
    max_input_phase_current = Ip/N

    l_coil = 2*(l+(pi*end_winding_radius))*N
    l_total = (Q/3)*l_coil

    Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)
    max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical  
  
  if debug == 1:
    print("\n(2) Maximum number of turns allowed N = %d turns"%N)
    print("(2) Peak voltage induced by all turns V_a(ln) = %f [V]"%max_induced_phase_voltage)
    print("(2) Peak input phase current Ia = %f [A]"%max_input_phase_current)
    print("(2) Ra_analytical = %f [Ohm]"%Ra_analytical)
    print("(2) Peak system input phase voltage V_a(ln) = %f [V]"%max_input_phase_voltage)
  
  #introduce contingency for 1 less turn during construction, thereby reducing the voltage.
  if enable_voltage_leeway == 1:
    N = N - 1
    turnsPerCoil = N
    
    area_available_per_turn = (coil_side_area*fill_factor*1000**2)/N
    max_induced_phase_voltage = N*EMK_PM
    max_input_phase_current = Ip/N	

    l_coil = 2*(l+(pi*end_winding_radius))*N
    l_total = (coilsTotal/3)*l_coil

    Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)
    max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical
    
    if debug == 1:
      print("\n(3) Maximum number of turns allowed N = %d turns"%N)
      print("(3) Peak voltage induced by all turns V_a(ln) = %f [V]"%max_induced_phase_voltage)
      print("(3) Peak input phase current Ia = %f [A]"%max_input_phase_current)
      print("(3) Ra_analytical = %f [Ohm]"%Ra_analytical)
      print("(3) Peak system input phase voltage V_a(ln) = %f [V]"%max_input_phase_voltage)    


  P_conductive_analytical = (3/2)*(max_input_phase_current**2)*Ra_analytical
  Ip = max_input_phase_current
  if debug == 1:
    print("(3) Total analytical conductive (copper) losses P_cu = %f [W]\n"%(P_conductive_analytical)) 

  
  Tm_lorentz=np.array([])
  if optimise_interface != 1:
    Tm_lorentz_simple=np.array([])

  def Tm_lorentz_simple_def(t):
    tmp=0
    for i, m in enumerate(m_PM_range):
    #for i, m in enumerate(m_range):
      if m in (3*m_AR_range-2):
      #if m in (3*m_range-2):
	#tmp+=(3*q*rn*N*l*Ip/(a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*bm_Br_PM_def(i,m,rn,III)*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
	#TODO: rn or rn/rn
	tmp+=(3*q*(rn)*N*l*Ip/(a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*bm_Br_PM_def(i,m,(rn/rn),III)*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
      elif m in (3*m_AR_range-1):
      #elif m in (3*m_range-1):
	#tmp+=(3*q*rn*N*l*Ip/(a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*bm_Br_PM_def(i,m,rn,III)*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)-np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
	#TODO: rn or rn/rn
	tmp+=(3*q*(rn)*N*l*Ip/(a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*bm_Br_PM_def(i,m,(rn/rn),III)*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)-np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
    return tmp

  def Tm_lorentz_def(t):
    tmp=0
    for i, m in enumerate(m_PM_range):
    #for i, m in enumerate(m_range):
      if m in (3*m_AR_range-2):
      #if m in (3*m_range-2):
	#tmp+=(3*q*N*l*Ip/(rn*hc*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,rn+hc/2)-int_bm_Br_III_PM_def(i,m,rn-hc/2))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
	#TODO: rn or rn/rn
	#tmp+=(3*q*N*l*Ip/(((rn*hc)/rn)*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,(rn+hc/2)/rn)-int_bm_Br_III_PM_def(i,m,(rn-hc/2)/rn))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))	
	#tmp+=(3*q*N*rn*rn*l*Ip/(((rn*hc)/rn)*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,(rn+hc/2)/rn)-int_bm_Br_III_PM_def(i,m,(rn-hc/2)/rn))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
	#tmp+=((3*q*rn**3*N*l*Ip)/(rn*hc*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,(rn+hc/2)/rn)-int_bm_Br_III_PM_def(i,m,(rn-hc/2)/rn))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
	tmp+=((3*q*rn**2*N*l*Ip)/(((rn*hc)/rn)*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,(rn+hc/2)/rn)-int_bm_Br_III_PM_def(i,m,(rn-hc/2)/rn))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
	#tmp+=(3*q*N*l*Ip/(((rn*hc)/rn)*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,(rn+hc/2))-int_bm_Br_III_PM_def(i,m,(rn-hc/2)))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
      elif m in (3*m_AR_range-1):
      #elif m in (3*m_range-1):
	#tmp+=(3*q*N*l*Ip/(rn*hc*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,rn+hc/2)-int_bm_Br_III_PM_def(i,m,rn-hc/2))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)-np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
	#tmp+=(3*q*N*l*Ip/(((rn*hc)/rn)*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,(rn+hc/2)/rn)-int_bm_Br_III_PM_def(i,m,(rn-hc/2)/rn))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)-np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
	#tmp+=(3*q*rn*rn*N*l*Ip/(((rn*hc)/rn)*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,(rn+hc/2)/rn)-int_bm_Br_III_PM_def(i,m,(rn-hc/2)/rn))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)-np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
	#tmp+=((3*q*rn**3*N*l*Ip)/(rn*hc*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,(rn+hc/2)/rn)-int_bm_Br_III_PM_def(i,m,(rn-hc/2)/rn))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)-np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
	tmp+=((3*q*rn**2*N*l*Ip)/(((rn*hc)/rn)*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,(rn+hc/2)/rn)-int_bm_Br_III_PM_def(i,m,(rn-hc/2)/rn))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)-np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
	#tmp+=(3*q*N*l*Ip/(((rn*hc)/rn)*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,(rn+hc/2))-int_bm_Br_III_PM_def(i,m,(rn-hc/2)))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)-np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
    return tmp

  for t in t_range:
    if optimise_interface != 1:
      #Analytical Flux-linkage calculations for each phase as function of time due to the Permanent Magnets
      lambda_a_PM=lambda_PM*np.sin(omega_m*p*t_range)
      lambda_b_PM=lambda_PM*np.sin(omega_m*p*t_range-2*pi/3)
      lambda_c_PM=lambda_PM*np.sin(omega_m*p*t_range-4*pi/3)

      lambda_a_PM_approx=lambda_PM_approx*np.sin(omega_m*p*t_range)
      lambda_b_PM_approx=lambda_PM_approx*np.sin(omega_m*p*t_range-2*pi/3)
      lambda_c_PM_approx=lambda_PM_approx*np.sin(omega_m*p*t_range-4*pi/3)

      #Analytical Flux-linkage calculations for each phase as function of time due to the Armature Reaction
      lambda_a_AR=lambda_AR*np.cos(2*pi*f_e*t_range)
      lambda_b_AR=lambda_AR*np.cos(2*pi*f_e*t_range-2*pi/3)
      lambda_c_AR=lambda_AR*np.cos(2*pi*f_e*t_range-4*pi/3)

      e_a_PM=EMK_PM*np.cos(omega_m*p*t_range)
      e_b_PM=EMK_PM*np.cos(omega_m*p*t_range-2*pi/3)
      e_c_PM=EMK_PM*np.cos(omega_m*p*t_range-4*pi/3)

      e_a_PM_approx=EMK_PM_approx*np.cos(omega_m*p*t_range)
      e_b_PM_approx=EMK_PM_approx*np.cos(omega_m*p*t_range-2*pi/3)
      e_c_PM_approx=EMK_PM_approx*np.cos(omega_m*p*t_range-4*pi/3)

      e_a_AR=-EMK_AR*np.sin(omega_m*p*t_range)
      e_b_AR=-EMK_AR*np.sin(omega_m*p*t_range-2*pi/3)
      e_c_AR=-EMK_AR*np.sin(omega_m*p*t_range-4*pi/3)

      dphi=phi_range[1]-phi_range[0]

  ####################
  # Determine torque #
  ####################

    #Tm_maxwell=np.append(Tm_maxwell,dphi*rn*l*hc/mu0*(Br_PM_def(rn,t,III)*Bt_AR_def(rn,t,III)-Br_AR_def(rn,t,III)*Bt_PM_def(rn,t,III)).sum())
    #FIXME: must be "+Br_AR_def(..."

    #Tm_lorentz=np.append(Tm_lorentz,(dphi*q*rn**2*hc*l*Jz_def(Ip,t)*Br_PM_def(rn/rn,t,III)).sum()) #TODO: rn or rn/rn
    Tm_lorentz=np.append(Tm_lorentz,Tm_lorentz_def(t)) #ahh, ek het altyd gewonder of mens kan append met gewone numpy arrays ook!
    
    #print(Tm_lorentz_def(t))
    if optimise_interface != 1:
      Tm_lorentz_simple=np.append(Tm_lorentz_simple,Tm_lorentz_simple_def(t))

  if optimise_interface != 1:
    #Calculate the Harmonic content of Tm_lorentz
    M=np.size(Tm_lorentz)-1
    #print(Tm_lorentz)
    Tmh_lorentz=np.abs(np.fft.fft(Tm_lorentz,M)/M)
    Tm0_lorentz=Tmh_lorentz[0]
    #print M
    #print(int(M/2))
    #print(Tmh_lorentz[2:int(M/2)])
  
    Tm_lorentz_THD=np.sqrt(np.sum(Tmh_lorentz[2:int((M)/2)]**2))/Tm0_lorentz

    #Calculate the Harmonic content of Tm_lorentz_simple
    
    M=np.size(Tm_lorentz_simple)-1
    Tmh_lorentz_simple=np.abs(np.fft.fft(Tm_lorentz_simple,M)/M)
    Tm0_lorentz_simple=Tmh_lorentz_simple[0]
    Tm_lorentz_simple_THD=np.sqrt(np.sum(Tmh_lorentz_simple[2:int((M)/2)]**2))/Tm0_lorentz_simple
    
    #Convert 's' into 'ms'
    t_range=t_range*1000
    T=T*1000

    #Average Torque calculations - Take I
    Tm=3/2*EMK_PM*Ip/omega_m

    #Average Torque calculations - Take II
    kT=kT_def(1/kq)
    Tm_kT=kT*Ip

    #print "Tm_lorentz_simple"
    #print Tm_lorentz_simple
    #print "Tm_lorentz"
    #print Tm_lorentz

  if optimise_interface != 1:
    Tm_lorentz_simple_mean=Tm_lorentz_simple.mean()
  
  Tm_lorentz_mean=Tm_lorentz.mean()

  #print "Tm_lorentz_simple"
  #print Tm_lorentz_simple
  #print "Tm_lorentz_simple_mean"
  #print Tm_lorentz_simple_mean
  
  if optimise_interface != 1:
    Tm_lorentz_simple_ripple=max(Tm_lorentz_simple-Tm_lorentz_simple_mean)-min(Tm_lorentz_simple-Tm_lorentz_simple_mean)
    Tm_lorentz_ripple=max(Tm_lorentz-Tm_lorentz_mean)-min(Tm_lorentz-Tm_lorentz_mean)

  if debug == 1:
    print('Tm=%g [Nm]' % (Tm))
    print('Tm|Lorentz_simple.mean=%g or %g [Nm]' % (Tm_lorentz_simple_mean, Tm0_lorentz_simple))
    print('Tm|Lorentz_simple.ripple=%g [Nm] or %g%%' % (Tm_lorentz_simple_ripple,Tm_lorentz_simple_ripple/Tm_lorentz_simple_mean*100))
    print('Tm|Lorentz_simple.THD=%g %%' % (Tm_lorentz_simple_THD*100))

    print('Tm|Lorentz.mean=%g or %g [Nm]' % (Tm_lorentz_mean,Tm0_lorentz))
    print('Tm|Lorentz.ripple=%g [Nm] or %g%%' % (Tm_lorentz_ripple,Tm_lorentz_ripple/Tm_lorentz_mean*100))
    print('Tm|Lorentz.THD=%g %%' % (Tm_lorentz_THD*100))













  if optimise_interface != 1:
    plt.close('all')
    #################
    # Contour Plots #
    #################

    #change these values back to their original values before AR solutions were found
    #riii=rn-hc/2 
    #riv =rn+hc/2
    riii= innerMagnetRadiusB/rn
    riv = outerMagnetRadiusA/rn

    # Initialise x & y array
    x=np.zeros(np.shape(Az_PM))
    y=np.zeros(np.shape(Az_PM))

    # Convert r & phi values -> x & y for the polar plot representation
    for i, r in enumerate(r_range):
      x[i]=r*rn*np.cos(phi_range)
      y[i]=r*rn*np.sin(phi_range)

    #Function to draw the RFAPM machine in polar coordinates
    def draw_polar_machine(ax,draw_coils):
      inner_yoke=Wedge((0,0), rii*rn, -180/q, 180/q, width=hy_i, facecolor="grey", alpha=0.5)
      outer_yoke=Wedge((0,0), rvi*rn, -180/q, 180/q, width=hy_o, facecolor="grey", alpha=0.5)
      #Radial magnets
      inner_S_magnet_0=Wedge((0,0), riii*rn, degrees(beta),       -degrees(beta)+180/p, width=hmi_r, facecolor="red",  alpha=0.25)
      outer_S_magnet_0=Wedge((0,0), rv*rn,   degrees(beta),       -degrees(beta)+180/p, width=hmo_r, facecolor="red", alpha=0.25)
      inner_N_magnet_0=Wedge((0,0), riii*rn, degrees(beta)-180/p, -degrees(beta),       width=hmi_r, facecolor="blue", alpha=0.25)
      outer_N_magnet_0=Wedge((0,0), rv*rn,   degrees(beta)-180/p, -degrees(beta),       width=hmo_r, facecolor="blue",  alpha=0.25)
      #Azimuthal magnets
      inner_CW_magnet_0=Wedge((0,0), riii*rn,-degrees(beta),        degrees(beta),       width=hmi_r, facecolor="red",  alpha=0.25)
      outer_CCW_magnet_0=Wedge((0,0), rv*rn,  -degrees(beta),        degrees(beta),       width=hmo_r, facecolor="blue",  alpha=0.25)
      inner_CCW_magnet_0=Wedge((0,0), riii*rn,-degrees(beta)-180/p,        degrees(beta)-180/p,       width=hmi_r, facecolor="blue",  alpha=0.25)
      outer_CW_magnet_0=Wedge((0,0), rv*rn,-degrees(beta)-180/p,        degrees(beta)-180/p,       width=hmo_r, facecolor="red",  alpha=0.25)
      inner_CCW_magnet_1=Wedge((0,0), riii*rn,-degrees(beta)+180/p,        degrees(beta)+180/p,       width=hmi_r, facecolor="blue",  alpha=0.25)
      outer_CW_magnet_1=Wedge((0,0), rv*rn,-degrees(beta)+180/p,        degrees(beta)+180/p,       width=hmo_r, facecolor="red",  alpha=0.25)
      if p!=q:
	#Radial magnets
	inner_S_magnet_1=Wedge((0,0), riii*rn, degrees(beta)-360/p, -degrees(beta)-180/p, width=hmi_r, facecolor="red",  alpha=0.25)
	outer_S_magnet_1=Wedge((0,0), rv*rn,   degrees(beta)-360/p, -degrees(beta)-180/p, width=hmo_r, facecolor="red", alpha=0.25)
	inner_N_magnet_1=Wedge((0,0), riii*rn, degrees(beta)+180/p, -degrees(beta)+360/p, width=hmi_r, facecolor="blue", alpha=0.25)
	outer_N_magnet_1=Wedge((0,0), rv*rn,   degrees(beta)+180/p, -degrees(beta)+360/p, width=hmo_r, facecolor="blue",  alpha=0.25)
	#Azimuthal magnets
	inner_CW_magnet_1=Wedge((0,0), riii*rn,-degrees(beta)+360/p,min(180/q,degrees(beta)+360/p),       width=hmi_r, facecolor="red",  alpha=0.25)
	outer_CCW_magnet_1=Wedge((0,0), rv*rn,  -degrees(beta)+360/p,min(180/q,degrees(beta)+360/p),       width=hmo_r, facecolor="blue",  alpha=0.25)
	inner_CW_magnet_2=Wedge((0,0), riii*rn,max(-180/q,-degrees(beta)-360/p),degrees(beta)-360/p,       width=hmi_r, facecolor="red",  alpha=0.25)
	outer_CCW_magnet_2=Wedge((0,0), rv*rn,  max(-180/q,-degrees(beta)-360/p),degrees(beta)-360/p,       width=hmo_r, facecolor="blue",  alpha=0.25)
      if draw_coils:
	if Type=='O':
	  coil_phase_A_pos=Wedge((0,0), riv*rn, -degrees(Delta/q)-90/q,     degrees(Delta/q)-90/q,    width=hc, facecolor="red",    alpha=0.5)
	  coil_phase_A_neg=Wedge((0,0), riv*rn, -degrees(Delta/q)+90/q,     degrees(Delta/q)+90/q,    width=hc, facecolor="red",    alpha=0.5)
	  coil_phase_B_pos=Wedge((0,0), riv*rn, -degrees(Delta/q)-150/q,    degrees(Delta/q)-150/q,   width=hc, facecolor="yellow", alpha=0.5)
	  coil_phase_B_neg=Wedge((0,0), riv*rn, -degrees(Delta/q)+30/q,     degrees(Delta/q)+30/q,    width=hc, facecolor="yellow", alpha=0.5)
	  coil_phase_C_pos=Wedge((0,0), riv*rn, -degrees(Delta/q)-30/q,     degrees(Delta/q)-30/q,    width=hc, facecolor="blue",   alpha=0.5)
	  coil_phase_C_neg=Wedge((0,0), riv*rn, -degrees(Delta/q)+150/q,    degrees(Delta/q)+150/q,   width=hc, facecolor="blue",   alpha=0.5)
	elif Type=='I':
	  coil_phase_A_pos=Wedge((0,0), riv*rn, -degrees(Delta/q)-30/q,     degrees(Delta/q)-30/q,    width=hc, facecolor="red",    alpha=0.5)
	  coil_phase_A_neg=Wedge((0,0), riv*rn, -degrees(Delta/q)+30/q,     degrees(Delta/q)+30/q,    width=hc, facecolor="red",    alpha=0.5)
	  coil_phase_B_pos=Wedge((0,0), riv*rn, -degrees(Delta/q)+90/q,     degrees(Delta/q)+90/q,    width=hc, facecolor="yellow", alpha=0.5)
	  coil_phase_B_neg=Wedge((0,0), riv*rn, -degrees(Delta/q)+150/q,    degrees(Delta/q)+150/q,   width=hc, facecolor="yellow", alpha=0.5)
	  coil_phase_C_pos=Wedge((0,0), riv*rn, -degrees(Delta/q)-150/q,    degrees(Delta/q)-150/q,   width=hc, facecolor="blue",   alpha=0.5)
	  coil_phase_C_neg=Wedge((0,0), riv*rn, -degrees(Delta/q)-90/q,     degrees(Delta/q)-90/q,    width=hc, facecolor="blue",   alpha=0.5)
	elif Type=='II':
	  coil_phase_A_pos=Wedge((0,0), (riv*rn)-g, -60/q,                     -60/q+2*degrees(Delta/q),  width=hc, facecolor="red",    alpha=0.5)
	  coil_phase_A_neg=Wedge((0,0), (riv*rn)-g,  60/q-2*degrees(Delta/q),   60/q,                     width=hc, facecolor="red",    alpha=0.5)
	  coil_phase_B_pos=Wedge((0,0), (riv*rn)-g,  60/q,                      60/q+2*degrees(Delta/q),  width=hc, facecolor="yellow", alpha=0.5)
	  coil_phase_B_neg=Wedge((0,0), (riv*rn)-g,  180/q-2*degrees(Delta/q),  180/q,                    width=hc, facecolor="yellow", alpha=0.5)
	  coil_phase_C_pos=Wedge((0,0), (riv*rn)-g, -180/q,                    -180/q+2*degrees(Delta/q), width=hc, facecolor="blue",   alpha=0.5)
	  coil_phase_C_neg=Wedge((0,0), (riv*rn)-g, -60/q-2*degrees(Delta/q),  -60/q,                     width=hc, facecolor="blue",   alpha=0.5)
	else:
	  raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."
      ax.add_patch(inner_yoke)
      ax.add_patch(outer_yoke)
      ax.add_patch(inner_N_magnet_0)
      ax.add_patch(outer_S_magnet_0)
      ax.add_patch(inner_S_magnet_0)
      ax.add_patch(outer_N_magnet_0)
      ax.add_patch(inner_CW_magnet_0)
      ax.add_patch(outer_CCW_magnet_0)
      ax.add_patch(inner_CCW_magnet_0)
      ax.add_patch(outer_CW_magnet_0)
      ax.add_patch(inner_CW_magnet_1)
      ax.add_patch(outer_CCW_magnet_1)
      ax.add_patch(inner_CCW_magnet_1)
      ax.add_patch(outer_CW_magnet_1)
      ax.add_patch(inner_CW_magnet_2)
      ax.add_patch(outer_CCW_magnet_2)
      if p!=q:
	ax.add_patch(inner_N_magnet_1)
	ax.add_patch(outer_S_magnet_1)
	ax.add_patch(inner_S_magnet_1)
	ax.add_patch(outer_N_magnet_1)
      if draw_coils:
	ax.add_patch(coil_phase_A_pos)
	ax.add_patch(coil_phase_A_neg)
	ax.add_patch(coil_phase_B_pos)
	ax.add_patch(coil_phase_B_neg)
	ax.add_patch(coil_phase_C_pos)
	ax.add_patch(coil_phase_C_neg)
      ax.set_aspect('equal')


    if show_figs or save_figs == True:
      ###############################################################
      # Plot Magnetic Vector Potential contour plot in polar format #
      ###############################################################
      if plot_PM:
	plt.figure(figsize=contour_figsize)
	CS=plt.contour(x,y,Az_PM*1000,40)
	draw_polar_machine(plt.gca(),draw_coils)
	plt.clabel(CS, inline=True, fontsize=5, fmt='%g')
	cb=plt.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%+5.1f')
	cb.set_label(r'Magnetic vector potential, $A_{z}$ [mWb/m]')
	#plt.axis(xmin=0.05,xmax=0.2,ymin=-0.08,ymax=0.08)
	plt.xlabel(r'$x$ [m]')
	plt.ylabel(r'$y$ [m]')
	if not save_figs:
	  plt.title(r"Contour plot of the PM's Magnetic Vector Potential")
	else:
	  plt.savefig('./pdf/Az_xy.pdf', transparent=True, bbox_inches='tight')

      if plot_AR:
	plt.figure(figsize=contour_figsize)
	CS=plt.contour(x,y,Az_AR*1000,40)
	draw_polar_machine(plt.gca(),draw_coils=True)
	plt.clabel(CS, inline=True, fontsize=5, fmt='%g')
	cb=plt.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%+5.1f')
	cb.set_label(r'Magnetic Vector potential, $A_{z|AR}$ [mWb/m]')
	plt.xlabel(r'$x$ [m]')
	plt.ylabel(r'$y$ [m]')
	if not save_figs:
	  plt.title(r"Contour plot of the Windings' Magnetic Vector Potential")
	else:
	  plt.savefig('./pdf/Az_xy_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')
	
	

      ###########################################################
      # Plot Magnetic Flux Density contour plot in polar format #
      ###########################################################
      if plot_PM:
	plt.figure(figsize=contour_figsize)
	CS=plt.contour(x,y,Bmag_PM,40)
	CS=plt.contourf(x,y,Bmag_PM,40)
	draw_polar_machine(plt.gca(),draw_coils)
	cb=plt.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%5.2f')
	cb.set_label(r'Flux density, $B_{mag}$ [T]')
	plt.xlabel(r'$x$ [m]')
	plt.ylabel(r'$y$ [m]')
	if not save_figs:
	  plt.title(r"Contour plot of the PM's Magnetic Flux Density Magnitude")
	else:
	  plt.savefig('./pdf/Bmag_xy.pdf',transparent=True, bbox_inches='tight')

      if plot_AR:
	plt.figure(figsize=contour_figsize)
	CS=plt.contour(x,y,Bmag_AR*1000,40)
	CS=plt.contourf(x,y,Bmag_AR*1000,40)
	draw_polar_machine(plt.gca(),draw_coils=True)
	cb=plt.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%5.1f')
	cb.set_label(r'Flux density, $B_{|AR}$ [mT]')
	plt.xlabel(r'$x$ [m]')
	plt.ylabel(r'$y$ [m]')
	if not save_figs:
	  plt.title(r"Contour plot of the Windings' Magnetic Flux Density Magnitude")
	else:
	  plt.savefig('./pdf/Bmag_xy_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      if linear_repr:
	##############################################################################
	# Function to draw the RFAPM machine in a linearised representational format #
	##############################################################################
	def draw_linearised_machine(ax):
	  inner_yoke=Rectangle((-180/q, ri*rn), 360/q, hy_i, facecolor="grey", alpha=0.5)
	  outer_yoke=Rectangle((-180/q, rv*rn), 360/q, hy_o, facecolor="grey", alpha=0.5)
	  inner_S_magnet_0=Rectangle((degrees(beta)-180/p, rii*rn),    km*180/p, hmi_r, facecolor="red", alpha=0.25)
	  outer_S_magnet_0=Rectangle((degrees(beta)-180/p, riv*rn+g), km*180/p, hmo_r, facecolor="red",  alpha=0.25)
	  inner_N_magnet_0=Rectangle((degrees(beta),       rii*rn),    km*180/p, hmi_r, facecolor="blue",  alpha=0.25)
	  outer_N_magnet_0=Rectangle((degrees(beta),       riv*rn+g), km*180/p, hmo_r, facecolor="blue", alpha=0.25)
	  if p!=q:
	    inner_S_magnet_1=Rectangle((degrees(beta)-360/p, rii*rn),    km*180/p, hmi_r, facecolor="red",  alpha=0.25)
	    outer_S_magnet_1=Rectangle((degrees(beta)-360/p, riv*rn+g), km*180/p, hmo_r, facecolor="red", alpha=0.25)
	    inner_N_magnet_1=Rectangle((degrees(beta)+180/p, rii*rn),    km*180/p, hmi_r, facecolor="blue", alpha=0.25)
	    outer_N_magnet_1=Rectangle((degrees(beta)+180/p, riv*rn+g), km*180/p, hmo_r, facecolor="blue",  alpha=0.25)
	  if draw_coils:
	    if Type=='O':
	      coil_phase_A_pos=Rectangle(( -90/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="red",    alpha=0.5)
	      coil_phase_A_neg=Rectangle((  90/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="red",    alpha=0.5)
	      coil_phase_B_pos=Rectangle((-150/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="yellow", alpha=0.5)
	      coil_phase_B_neg=Rectangle((  30/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="yellow", alpha=0.5)
	      coil_phase_C_pos=Rectangle(( -30/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="blue",   alpha=0.5)
	      coil_phase_C_neg=Rectangle(( 150/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="blue",   alpha=0.5)
	    elif Type=='I':
	      coil_phase_A_pos=Rectangle(( -30/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="red",    alpha=0.5)
	      coil_phase_A_neg=Rectangle((  30/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="red",    alpha=0.5)
	      coil_phase_B_pos=Rectangle((  90/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="yellow", alpha=0.5)
	      coil_phase_B_neg=Rectangle(( 150/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="yellow", alpha=0.5)
	      coil_phase_C_pos=Rectangle((-150/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="blue",   alpha=0.5)
	      coil_phase_C_neg=Rectangle(( -90/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="blue",   alpha=0.5)
	    elif Type=='II':
	      coil_phase_A_pos=Rectangle(( -60/q,                    riii*rn),   2*degrees(Delta/q), hc,  facecolor="red",    alpha=0.5)
	      coil_phase_A_neg=Rectangle((  60/q-2*degrees(Delta/q), riii*rn),   2*degrees(Delta/q), hc,  facecolor="red",    alpha=0.5)
	      coil_phase_B_pos=Rectangle((  60/q,                    riii*rn),   2*degrees(Delta/q), hc,  facecolor="yellow", alpha=0.5)
	      coil_phase_B_neg=Rectangle(( 180/q-2*degrees(Delta/q), riii*rn),   2*degrees(Delta/q), hc,  facecolor="yellow", alpha=0.5)
	      coil_phase_C_pos=Rectangle((-180/q,                    riii*rn),   2*degrees(Delta/q), hc,  facecolor="blue",   alpha=0.5)
	      coil_phase_C_neg=Rectangle(( -60/q-2*degrees(Delta/q), riii*rn),   2*degrees(Delta/q), hc,  facecolor="blue",   alpha=0.5)
	    else:
	      raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."
	  ax.add_patch(inner_yoke)
	  ax.add_patch(outer_yoke)
	  ax.add_patch(inner_N_magnet_0)
	  ax.add_patch(outer_S_magnet_0)
	  ax.add_patch(inner_S_magnet_0)
	  ax.add_patch(outer_N_magnet_0)
	  if p!=q:
	    ax.add_patch(inner_N_magnet_1)
	    ax.add_patch(outer_S_magnet_1)
	    ax.add_patch(inner_S_magnet_1)
	    ax.add_patch(outer_N_magnet_1)
	  if draw_coils:
	    ax.add_patch(coil_phase_A_pos)
	    ax.add_patch(coil_phase_A_neg)
	    ax.add_patch(coil_phase_B_pos)
	    ax.add_patch(coil_phase_B_neg)
	    ax.add_patch(coil_phase_C_pos)
	    ax.add_patch(coil_phase_C_neg)
	  ax.set_aspect('equal')

	#Meshgrid for linearised contour plot
	phi,r=np.meshgrid(np.degrees(phi_range),r_range)

      if plot_PM and linear_repr:
	#################################################################
	# Plot Magnetic Vector Potential contour plot in rectangular format #
	#################################################################
	plt.figure()
	CS=plt.contour(phi,r,Az_PM,50)
	ax=plt.gca()
	draw_linearised_machine(ax)
	ax.set_xticks(np.linspace(-180/q,180/q,9))
	ax.set_yticks(np.linspace(ri*rn,rvi*rn,7))
	plt.axis([-180/q,180/q,ri*rn,rvi*rn])
	plt.clabel(CS, inline=True, fontsize=5, fmt='%g')
	plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
	plt.ylabel(r'$r$ [m]')
	if not save_figs:
	    plt.title(r"Contour plot of the PM's Magnetic Vector Potential")
	else:
	  plt.savefig('./pdf/Az_rt_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      if plot_AR and linear_repr:
	#################################################################
	# Plot Magnetic Vector Potential contour plot in rectangular format #
	#################################################################
	plt.figure()
	CS=plt.contour(phi,r,Az_AR,50)
	ax=plt.gca()
	draw_linearised_machine(ax)
	ax.set_xticks(np.linspace(-180/q,180/q,9))
	ax.set_yticks(np.linspace(ri*rn,rvi*rn,7))
	plt.axis([-180/q,180/q,ri*rn,rvi*rn])
	plt.clabel(CS, inline=True, fontsize=5, fmt='%g')
	plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
	plt.ylabel(r'$r$ [m]')
	if not save_figs:
	    plt.title(r"Contour plot of the Windings' Magnetic Vector Potential")
	else:
	  plt.savefig('./pdf/Az_rt_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      if plot_PM and linear_repr:
	#################################################################
	# Plot Magnetic Flux Density contour plot in rectangular format #
	#################################################################
	plt.figure()
	CS=plt.contour(phi,r,Bmag_PM,40)
	CS=plt.contourf(phi,r,Bmag_PM,50)
	ax=plt.gca()
	draw_linearised_machine(ax)
	ax.set_xticks(np.linspace(-180/q,180/q,9))
	ax.set_yticks(np.linspace(ri*rn,rvi*rn,7))
	plt.axis([-180/q,180/q,ri*rn,rvi*rn])
	plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
	plt.ylabel(r'$r$ [m]')
	if not save_figs:
	  plt.title(r"Contour plot of the PM's Magnetic Flux Density Magnitude")
	else:
	  plt.savefig('./pdf/Bmag_rt_PM.pdf')

      if plot_AR and linear_repr:
	#################################################################
	# Plot Magnetic Flux Density contour plot in rectangular format #
	#################################################################
	plt.figure()
	CS=plt.contour(phi,r,Bmag_AR,40)
	CS=plt.contourf(phi,r,Bmag_AR,50)
	ax=plt.gca()
	draw_linearised_machine(ax)
	ax.set_xticks(np.linspace(-180/q,180/q,9))
	ax.set_yticks(np.linspace(ri*rn,rvi*rn,7))
	plt.axis([-180/q,180/q,ri*rn,rvi*rn])
	plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
	plt.ylabel(r'$r$ [m]')
	if not save_figs:
	  plt.title(r"Contour plot of the Windings' Magnetic Flux Density Magnitude")
	else:
	  plt.savefig('./pdf/Bmag_rt_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      if plot_current_density:
	#####################################
	# Plot Current Density Distribution #
	#####################################
	plt.figure()
	plt.plot(np.degrees(phi_range),Jz_def(Ip,t))
	ax=plt.gca()
	ax.set_xticks(np.linspace(-180/q,180/q,9))
	plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
	plt.ylabel(r'$J_z$ [A$/$m$^2$]')
	plt.axis(xmin=-180/q,xmax=180/q)
	plt.grid(True)
	if not save_figs:
	  plt.title('Current Density Distribution')
	else:
	  plt.savefig('./pdf/Jz-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      ##########################
      # Plot the Flux-linkages #
      ##########################
      #Print important flux-linkage and induced voltage info
      if debug == 1:
	print('lambda_PM=%g [Wbt]' % (abs(lambda_PM)))
	print('lambda_PM_approx=%g [Wbt]' % (lambda_PM_approx))    
      
      plt.figure()
      plt.plot(t_range,lambda_a_PM,label=r'$\lambda_{a|PM}(t)$')
      plt.plot(t_range,lambda_b_PM,label=r'$\lambda_{b|PM}(t)$')
      plt.plot(t_range,lambda_c_PM,label=r'$\lambda_{c|PM}(t)$')

      ax=plt.gca()
      ax.set_xticks(np.linspace(0,T,11))
      plt.xlabel(r'Time, $t$ [ms]')
      plt.ylabel(r'Flux linkage, $\lambda_{|PM}$ [Wbt]')
      plt.legend()
      #plt.axis(xmin=0,xmax=T,ymin=-2,ymax=2)
      plt.grid(True)
      if not save_figs:
	plt.title('Flux-linkage due to the Permanent Magnets')
      else:
	plt.savefig('./pdf/lambda_abc_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      if plot_approx:
	#Add the flux-linkage approximations
	plt.figure()
	plt.plot(t_range,lambda_a_PM_approx,label=r'$\lambda_{a|PM}(t)$ (Approx.)')
	plt.plot(t_range,lambda_b_PM_approx,label=r'$\lambda_{b|PM}(t)$ (Approx.)')
	plt.plot(t_range,lambda_c_PM_approx,label=r'$\lambda_{c|PM}(t)$ (Approx.)')

	ax=plt.gca()
	ax.set_xticks(np.linspace(0,T,11))
	plt.xlabel(r'Time, $t$ [ms]')
	plt.ylabel(r'Flux linkage, $\lambda_{|PM}$ [Wbt]')
	plt.legend()
	#plt.axis(xmin=0,xmax=T,ymin=-2,ymax=2)
	plt.grid(True)
	if not save_figs:
	  plt.title('Approximate Flux-linkage due to the Permanent Magnets')
	else:
	  plt.savefig('./pdf/lambda_abc_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      #########################
      # Plot Induced Voltages #
      #########################
      plt.figure()
      plt.plot(t_range,e_a_PM,linewidth=1,label=r'$e_{a|PM}(t)$')
      plt.plot(t_range,e_b_PM,linewidth=1,label=r'$e_{b|PM}(t)$')
      plt.plot(t_range,e_c_PM,linewidth=1,label=r'$e_{c|PM}(t)$')

      ax=plt.gca()
      ax.set_xticks(np.linspace(0,T,11))
      plt.xlabel(r'Time, $t$ [ms]')
      plt.ylabel(r'Back EMF, $e_{|PM}$ [V]')
      plt.legend()
      #plt.axis(xmin=0,xmax=T,ymin=-1000,ymax=1000)
      plt.axis(xmin=0,xmax=T)
      plt.grid(True)
      if not save_figs:
	plt.title('Induced Voltages due to the Permanent Magnets')
      else:
	plt.savefig('./pdf/e_abc_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      if plot_approx:
	#Add the induced voltage approximations
	plt.figure()
	plt.plot(t_range,e_a_PM_approx,linewidth=1,label=r'$e_{a|PM}(t)$ (Approx.)')
	plt.plot(t_range,e_b_PM_approx,linewidth=1,label=r'$e_{b|PM}(t)$ (Approx.)')
	plt.plot(t_range,e_c_PM_approx,linewidth=1,label=r'$e_{c|PM}(t)$ (Approx.)')

	ax=plt.gca()
	ax.set_xticks(np.linspace(0,T,11))
	plt.xlabel(r'Time, $t$ [ms]')
	plt.ylabel(r'Back EMF, $e_{|PM}$ [V]')
	plt.legend()
	#plt.axis(xmin=0,xmax=T,ymin=-1000,ymax=1000)
	plt.axis(xmin=0,xmax=T)
	plt.grid(True)
	if not save_figs:
	  plt.title('Induced Voltages due to the Permanent Magnets')
	else:
	  plt.savefig('./pdf/e_abc_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      if plot_AR:
	############################################################################
	# Plot the Flux-linkages and Induced Voltages due to the Armature Reaction #
	############################################################################
	#Plot the Flux-linkages
	plt.figure()
	plt.plot(t_range,lambda_a_AR,label=r'$\lambda_{a|AR}(t)$')
	plt.plot(t_range,lambda_b_AR,label=r'$\lambda_{b|AR}(t)$')
	plt.plot(t_range,lambda_c_AR,label=r'$\lambda_{c|AR}(t)$')
	ax=plt.gca()
	ax.set_xticks(np.linspace(0,T,11))
	plt.xlabel(r'Time, $t$ [ms]')
	plt.ylabel(r'Flux-linkage, $\lambda$ [Wbt]')
	plt.legend()
	#plt.axis(xmin=0,xmax=T,ymax=0.06,ymin=-0.06)
	plt.axis(xmin=0,xmax=T)
	plt.grid(True)
	if not save_figs:
	  plt.title('Flux-linkage due to the Armature Windings')
	else:
	  plt.savefig('./pdf/lambda_abc_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

	#Plot the Induced Voltages
	plt.figure()
	plt.plot(t_range,e_a_AR,label=r'$e_{a|AR}(t)$')
	plt.plot(t_range,e_b_AR,label=r'$e_{b|AR}(t)$')
	plt.plot(t_range,e_c_AR,label=r'$e_{c|AR}(t)$')
	ax=plt.gca()
	ax.set_xticks(np.linspace(0,T,11))
	plt.xlabel(r'Time, $t$ [ms]')
	plt.ylabel(r'$e(t)$~[V]')
	plt.legend()
	plt.axis(xmin=0,xmax=T)
	plt.grid(True)
	if not save_figs:
	  plt.title('Induced Voltages due to the Armature Windings')
	else:
	  plt.savefig('./pdf/e_abc_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')




      if plot_PM:
	#####################################################################################
	# Plot the Radial Flux Density Variation in the Stator due to the Permanent Magnets #
	#####################################################################################
	plt.figure()
	#Br_semfem=np.loadtxt("../semfem/br.csv",skiprows=1,delimiter=",")
	#phi_range_semfem=np.linspace(-pi/q,pi/q,np.size(Br_semfem[:,0]))

	#print "i_rnmh2"
	#print i_rnmh2
	#print r_range[i_rnmh2]
	#print "i_rn"
	#print i_rn
	#print r_range[i_rn]
	#print "i_rnph2"
	#print i_rnph2
	#print r_range[i_rnph2]
	
	plt.plot(np.degrees(phi_range),Br_PM[i_rnmh2],'b',linewidth=LINEWIDTH*2,label=r'$B_{r|r_n-h/2}$')
	#plt.plot(np.degrees(phi_range_semfem),Br_semfem[:,0],'b',linewidth=LINEWIDTH,label=r'$B_{r|r_n-h/2}$ \small (SEMFEM)')
	plt.plot(np.degrees(phi_range),Br_PM[i_rn],'g',linewidth=LINEWIDTH*2,label=r'$B_{r|r_{n}}$')
	#plt.plot(np.degrees(phi_range_semfem),Br_semfem[:,1],'g',linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$ \small  (SEMFEM)')
	plt.plot(np.degrees(phi_range),Br_PM[i_rnph2],'r',linewidth=LINEWIDTH*2,label=r'$B_{r|r_n+h/2}$')
	#plt.plot(np.degrees(phi_range_semfem),Br_semfem[:,2],'r',linewidth=LINEWIDTH,label=r'$B_{r|r_n+h/2}$ \small  (SEMFEM)')

	ax=plt.gca()
	ax.set_xticks(np.linspace(-180/q,180/q,9))
	plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
	plt.ylabel(r'Radial flux density, $B_{r}$ [T]')
	plt.legend(loc='upper left')
	plt.axis(xmin=-180/q,xmax=180/q)
	plt.grid(True)
	if not save_figs:
	  plt.title(r'$B_r(\phi)$ in stator due to the Permanent Magnets')
	else:
	  plt.savefig('./pdf/Br_phi.pdf',transparent=True, bbox_inches='tight')

      if plot_AR:
	########################################################################################################
	# Plot the Radial Flux Density Variation in the Stator and Magnet Centers due to the Armature Windings #
	######################################################################################################## 
	plt.figure()
	
	#print(np.shape(phi_range))
	#print(np.shape(Br_AR[i_rcim][0][0]))
	
	plt.plot(np.degrees(phi_range),Br_AR[i_rcim][0][0],label=r'$B_{r|r_{cim},AR}$') #FIXME: [0][0] was not orignally here
	plt.plot(np.degrees(phi_range),Br_AR[i_rn],label=r'$B_{r|r_{n},AR}$')
	plt.plot(np.degrees(phi_range),Br_AR[i_rcom][0][0],label=r'$B_{r|r_{com},AR}$') #FIXME: [0][0] was not orignally here

	ax=plt.gca()
	ax.set_xticks(np.linspace(-180/q,180/q,9))
	plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
	plt.ylabel(r'Radial flux density, $B_{r|AR}$ [T]')
	plt.legend(loc='upper left')
	plt.axis(xmin=-180/q,xmax=180/q)
	plt.grid(True)
	if not save_figs:
	  plt.title(r'$B_r(\phi)$ in stator and magnet centers due to the Armature windings')
	else:
	  plt.savefig('./pdf/Br_rt_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      if plot_PM:
	################################################################
	# Plot the Azimuthal Flux Density due to the Permanent Magnets #
	################################################################
	#Plot the Azimuthal Flux Density due to the Permanent Magnets
	plt.figure()
	plt.plot(np.degrees(phi_range),Bt_PM[i_rcim][0][0],label=r'$B_{\phi|r_{cim},PM}$')
	plt.plot(np.degrees(phi_range),Bt_PM[i_rn],label=r'$B_{\phi|r_{n},PM}$')
	plt.plot(np.degrees(phi_range),Bt_PM[i_rcom][0][0],label=r'$B_{\phi|r_{com},PM}$')
	
	ax=plt.gca()
	ax.set_xticks(np.linspace(-180/q,180/q,9))
	plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
	plt.ylabel(r'Azimuthal flux density, $B_{{\phi}|PM}$ [T]')
	plt.legend()
	plt.axis(xmin=-180/q,xmax=180/q)
	plt.grid(True)
	if not save_figs:
	  plt.title(r'$B_t(\phi)$ in stator and magnet centers due to the Permanent Magnets')
	else:
	  plt.savefig('./pdf/Bt_rt_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

	#TOTAL FLUX DENSITY MAGNITUDE IN YOKES
	#Plot the Magnitude of the Flux Density Variation in the Yokes due to the Permanent Magnets
	plt.figure()
	plt.plot(np.degrees(phi_range),np.sqrt(Br_PM[i_rciy][0][0]**2+Bt_PM[i_rciy][0][0]**2),label=r'$B_{mag|PM,r_{ciy}}$')
	plt.plot(np.degrees(phi_range),np.sqrt(Br_PM[i_rcoy][0][0]**2+Bt_PM[i_rcoy][0][0]**2),label=r'$B_{mag|PM,r_{coy}}$')

	ax=plt.gca()
	ax.set_xticks(np.linspace(-180/q,180/q,9))
	plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
	plt.ylabel(r'Magntitude of Flux density, $B_{mag|PM}$ [T]')
	plt.legend(loc='upper left')
	plt.axis(xmin=-180/q,xmax=180/q)
	plt.grid(True)
	if not save_figs:
	  plt.title(r'Magnitude of flux density $B_{mag}(\phi)$  in yokes due to the Permanent Magnets')
	else:
	  plt.savefig('./pdf/Bmag_yokes_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      if plot_torque:
	#if Type=='O':
	  #Tmin=290
	  #Tmax=340
	#elif Type=='I':
	  #Tmin=175
	  #Tmax=200
	#else:
	  #Tmin=200
	  #Tmax=225

	#Plot the Torque calculated using the simplified Lorentz's Method vs. Ansoft Maxwell 2D
	plt.figure()
	plt.plot(t_range,np.abs(Tm_lorentz_simple),'-',linewidth=1,label=r'Using the Simplified Lorentz Method')
	#plt.axis(xmin=0,xmax=T,ymin=Tmin,ymax=Tmax)
	if not save_figs: plt.title(r'Torque -- Type %s' % Type)
	plt.plot(t_range,np.ones(np.size(t_range))*Tm_kT,'-',linewidth=1,label=r'Simplified Average Torque Calculation')
	plt.xlabel(r'Time, $t$ [ms]')
	plt.ylabel(r'$T_{mech}$ [Nm]')
	plt.legend(loc='best')
	ax=plt.gca()
	ax.set_xticks(np.linspace(0,T,11))
	plt.grid(True)
	if not save_figs:
	  plt.title(r"Using the simplified Lorentz's method")
	else:
	  plt.savefig('./pdf/Tm_lorentz_simple-%s.pdf' % Type, transparent=True, bbox_inches='tight')

	#Plot the Torque calculated using the Lorentz's Method vs. Ansoft Maxwell 2D
	plt.figure()
	#print "np.shape"
	#print np.shape(t_range)
	#print np.shape(Tm_lorentz)
	plt.plot(t_range,np.abs(Tm_lorentz),'-',linewidth=1,label=r'Using the Lorentz Method')
	#plt.axis(xmin=0,xmax=T,ymin=Tmin,ymax=Tmax)
	if not save_figs: plt.title(r'Torque -- Type %s' % Type)
	plt.plot(t_range,np.ones(np.size(t_range))*Tm_kT,'-',linewidth=1,label=r'Simplified Average Torque Calculation')
	plt.xlabel(r'Time, $t$ (ms)')
	plt.ylabel(r'Mechanical Torque, $T_{mech}$ (Nm)')
	plt.legend(loc='best')
	ax=plt.gca()
	ax.set_xticks(np.linspace(0,T,11))
	plt.grid(True)
	if not save_figs:
	  plt.title(r"Using the Lorentz's Method")
	else:
	  plt.savefig('./pdf/Tm_lorentz-%s.pdf' % Type, transparent=True, bbox_inches='tight')


	#Plot the harmonics
	plt.figure()
	if Type=='II':
	  bar_width=0.25
	else:
	  bar_width=0.33
	h_range=np.arange(1,20)
	plt.bar(h_range,100*Tmh_lorentz[h_range]/abs(Tm_lorentz_mean),bar_width,color='b',label='Analytical')
	ax=plt.gca()
	ax.set_xticks(h_range)
	plt.grid(True)
	plt.xlabel(r'Harmonic Number')
	plt.ylabel(r'$\frac{T_{mech|h}}{T_{mech|ave.}}\times 100$~\%')
	plt.legend(loc='best')
	if not save_figs:
	  plt.title(r'$T_{mh}$')
	else:
	  plt.savefig('./pdf/Tmh-%s.pdf' % Type, transparent=True, bbox_inches='tight')

    
    if show_figs: plt.show()
  
  
  if optimise_interface != 1:
    K1=kq*sin(kDelta*pi/(6*kq))*kw_pitch_def(int(1/kq))

    if debug == 1:
      print('K1=%g' % K1)

    if Type=='O':
      le=2*pi*rn/q+2*h
    elif Type=='I':
      le=pi/Q*rn*pi/2
    elif Type=='II':
      le=(2*pi/Q-2*Delta/q)*rn*pi/2
    else:
      raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."

    ke=1+le/l

    K2=ke*kDelta

    if debug == 1:
      print('K2=%g' % K2)
    
    #############################
    # Print results to terminal #
    #############################
    if debug == 1:
      print('Brmax_PM|rn   = %g T' % Brmax_PM)			#all harmonics included peak value @ rn
      print('Br1_PM|rn     = %g T' % Br1_PM)			#1st harmonic peak value @ rn
      print('Br1_PM|ave    = %g T' % np.average(Br1_PM_range))	#average of 1st harmonic peak values from rn-hc/2 to rn+hc/2
      print('THD(Br_PM|rn) = %g %%' % (Br_PM_THD*100,))
      print('Done!')

    #debugging
    #print('Br_avg = %g T' % np.average(Br))

  #########################
  # Calculate magnet mass #
  #########################

  #Assuming NdFeB N48 grade magnets
  magnet_density=7500 #[kg/m^3]
  inner_magnets_mass = magnet_density*((np.pi*innerMagnetRadiusB**2)-(np.pi*innerMagnetRadiusA**2))*l
  outer_magnets_mass = magnet_density*((np.pi*outerMagnetRadiusB**2)-(np.pi*outerMagnetRadiusA**2))*l
  magnet_mass = inner_magnets_mass + outer_magnets_mass
  if debug == 1:
    print("inner_magnets_mass = %f [kg]"%inner_magnets_mass)
    print("outer_magnets_mass = %f [kg]"%outer_magnets_mass)
  
  #########################
  # Calculate copper mass #
  #########################

  copper_density=8950 #[kg/m^3] #some sources say 8940 and some say 8960
  single_coil_mass = copper_density*fill_factor*((coil_side_radians*CoilRadiusB**2) - (coil_side_radians*CoilRadiusA**2))*l
  copper_mass = Q*single_coil_mass
  
  ##################################
  # Calculate Power and Efficiency #
  ##################################
  P_core_loss = 0
  P_eddy = 0
  P_wf = 0
  P_losses = P_conductive_analytical + P_core_loss + P_eddy + P_wf
  
  P_out = (n_rpm*(pi/30))*Tm_lorentz_mean
  P_in = P_out + P_losses
  efficiency = (P_out/P_in)*100
  
  #################
  # Print results #
  #################
  addToTable("Max input phase current", max_input_phase_current, "[A]", True,dir_parent)
  addToTable("Max induced phase voltage",max_induced_phase_voltage, "[V]", True,dir_parent) 	#phase A max induced voltage
  addToTable("Max input phase voltage",max_input_phase_voltage, "[V]", True,dir_parent) 	#phase A max induced voltage
  addToTable("Max flux linkage",0, "[Wb]", True,dir_parent)
  addToTable("******Lorentz average torque produced******",Tm_lorentz_mean,"[Nm]", True,dir_parent)
  if optimise_interface != 1:
    addToTable("Simplified Lorentz average torque produced",Tm_lorentz_simple_mean,"[Nm]", True ,dir_parent)
  addToTable("*****************Magnet mass***************",magnet_mass,"[kg]", True,dir_parent)
  addToTable("Copper mass",copper_mass,"[kg]", True,dir_parent)
  addToTable("Phase resistance",Ra_analytical,"[Ohm]", True ,dir_parent)
  #if steps == 1:
  addToTable("Phase resistance (analytical)",Ra_analytical,"[Ohm]", True,dir_parent)
  #addToTable("Cogging torque",cogging,"[Nm]", True,dir_parent)
  addToTable("Electrical frequency",f_e,"[Hz]", True,dir_parent)
  addToTable("Machine speed",n_rpm,"[rpm]", True,dir_parent)
  addToTable("****************Efficiency****************",efficiency,"[%]", True,dir_parent)
  addToTable("Output Torque/Mass Ratio",(Tm_lorentz_mean/(magnet_mass+copper_mass)),"[Nm/kg]", True,dir_parent)
  addToTable("Output Torque/PM Mass Ratio",(Tm_lorentz_mean/magnet_mass),"[Nm/kg]", True,dir_parent)
  #addToTable("Input Apparant Power (S)",S,"[VA]", True,dir_parent)
  #if steps == 1:
  #addToTable("Input Reactive Power (Q)",Q,"[VAR]", True,dir_parent)
  #addToTable("Input Real Power (mean value of instantaneous power over full period) (P)",P,"[W]", steps != 1,dir_parent)
  addToTable("Input Real Power (P_in)",P_in,"[W]", True,dir_parent)
  #addToTable("Input Real Power (P)",P,"[W]", True,dir_parent)
  addToTable("Output Mechanical Power",P_out,"[W]", True,dir_parent)
  #if steps == 1:
  addToTable("Conductive loss (analytical with end-windings)",P_conductive_analytical,"[W]", True,dir_parent)
  #addToTable("Conductive loss (SEMFEM)",P_conductive_semfem,"[W]", True,dir_parent)
  #addToTable("Conductive loss (with end-windings)",P_conductive,"[W]", True,dir_parent)
  addToTable('Eddy current losses',P_eddy,'[W]', enable_eddy_losses_calc == 1,dir_parent)
  addToTable("Wind & Friction losses",P_wf,"[W]", True,dir_parent)
  addToTable("Core loss",P_core_loss,"[W]", True,dir_parent)					#core loss will be zero because there is no B-H hysteresis effect for an ironless machine  
  addToTable("Total losses",P_losses,"[W]", True,dir_parent)
  #if steps == 1:
  #addToTable("Power factor",power_factor,"[%]", True,dir_parent)
  addToTable("Fill factor",fill_factor,"[p.u.]",True,dir_parent)
  addToTable("Coil side area",coil_side_area*1000**2,"[mm^2]",True,dir_parent)
  #if steps == 1:
  #addToTable("Area available per Turn (including fill factor)",area_available_per_turn,"[mm^2]",True,dir_parent)
  #addToTable("Area required per Turn (Litz with parallel strands)",single_turn_area*1000**2,"[mm^2]",True,dir_parent)
  addToTable("Turns per coil (N)",N,"[Turns]",True,dir_parent)
  addToTable("Current through single slot (peak)",current_through_slot,"[A]",True,dir_parent)
  addToTable("Current density (peak)",J,"[A/mm^2]",True,dir_parent)
  addToTable("Coil temperature",coil_temperature,"[Deg. C]",True,dir_parent)
  #if steps == 1:
  addToTable("Coil winding length",l_coil,"[m]",True,dir_parent)
  addToTable("Phase winding length",l_total,"[m]",True,dir_parent)
  addToTable("Copper resistivity",wire_resistivity,"[Ohm.m]",True,dir_parent)
  
  #addToTable("Active slot area",active_slot_area*1000**2,"[mm^2]",True,dir_parent)
  #addToTable("Achieved fill factor",active_slot_area/coil_side_area,"[p.u.]",True,dir_parent)
      
#else:
  #addToTable("Estimated torque produced (dq quantities)",average_torque_dq,"[Nm]", True,dir_parent)
  

#if enable_flux_density_harmonic_calc == 1:
  #addToTable('Brmax|rn',max_Br_1,'[T]', True,dir_parent)
  #addToTable('Br1|rn',Br1,'[T]', True,dir_parent)
  #addToTable('THD(Br|rn)',(Br_THD*100),'[%]', True,dir_parent)


  #if enable_terminal_output == 1:
  save_stdout = sys.stdout
  sys.stdout = open(dir_parent+"pretty_terminal_table.txt", "wb")
  print pretty_terminal_table
  sys.stdout = save_stdout    
    
  if VERBOSITY == 1:
    print pretty_terminal_table
    if enable_eddy_losses_calc == 1:
      print pretty_eddy
      print pretty_layer
      pretty_eddy.clear_rows()
      pretty_layer.clear_rows()

  pretty_terminal_table.clear_rows()

  #if calculate_all_torques == 1:
    #print("Torque Lorentz simplified")
    #print("Torque Lorentz")

    #pretty_terminal_table = None
 
  #output_dat = open('./script_results/qhdrfapm/output.csv','wb')
  #output_dat.write('Brmax|rn, Br1|rn, THD(BR|rn)\n')
  #output_dat.write('%f,%f,%f'%(Brmax_PM,Br1_PM,Br_PM_THD*100)) 

  dir = dir_parent+'script_results/main/'
  if not os.path.isdir(dir): os.makedirs(dir)
  
  with open(dir_parent+'script_results/main/running_flags.csv', 'wb') as running_flags_file:
    running_flags_file.write("iteration_number,%d"%(iteration_number+1)) 
    
  if time:
    end_time = time.time()
    if enable_terminal_output == 1:
      print("Elapsed time is: %f"%(end_time-start_time))


if __name__ == '__main__': #when executed by itself
  main('input_original.csv')
