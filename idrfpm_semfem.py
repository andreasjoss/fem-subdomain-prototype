#!/usr/bin/python
#"The line above is needed because this script requires the python shell to execute the following command lines."

#Title	: Simulation of a Double-rotor Ironless Radial Flux Permanent Magnet Machine
#Author : Andreas Joss

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)

from semfem import *
import pylab as pl
from SEMFEM_plotter import *
from prettytable import PrettyTable
import time
#pyplot is just a wrapper module to provide a Matlab-style interface to matplotlib. 
#Many plotting functions in Matlab are provided by pyplot with the same names and arguments. 
#This will ease the process of moving from Matlab to Python for scientific computation.
#pylab is basically a mode in which pyplot and numpy are imported in a single namespace, 
#thus making the Python working environment very similar to Matlab.

from numpy import * # * imports all the functions contained within the numpy library
		  # this numpy library is necessary to use the loadtxt() function
import os, sys, csv

debug=0

#CONSTANTS
READ_INPUT = 1
MANUAL_INPUT = 2

MAGNETS_ONLY = 1
WINDINGS_ONLY = 2
ALL_ACTIVE = 3


pretty_terminal_table = PrettyTable(['SEMFEM Output Variable','Value','Unit'])
pretty_terminal_table.align = 'l'
pretty_terminal_table.border= True  
has_opened = False
enable_variable_test = False


def addToTable (variable_string_name, value, unit_string, enable_disable, dir_parent):
  if enable_disable == True:
    pretty_terminal_table.add_row([variable_string_name,value,unit_string])
    variable_string_name = variable_string_name.replace(" ", "_")
    with open(dir_parent+'script_results/idrfpm/output.csv',"a") as output_dat:
      output_dat.write('%s,%f\n'%(variable_string_name,value))

	
def main (*args):   
  #if len(args) > 1:
    #number_of_threads = int(args[1])
    
    #if number_of_threads > 1:
      #dir_parent = './mp/p%s/'%args[1]
    #else:
      #dir_parent = ''
  #else:
    #number_of_threads = 1
    #dir_parent = ''
    ##dir_parent = './mp/p%s/'%args[1]

  if __name__ != '__main__':
    dir_parent = './mp/p%s/'%args[1]
    number_of_threads = int(args[1])
  else:
    dir_parent = ''
    number_of_threads = 1
    
  #temp fix
  #dir_parent = './mp/p%s/'%args[1]
  #number_of_threads = int(args[1])
  
  print("writing to "+dir_parent+"script_results/idrfpm/output.csv")

  with open(dir_parent+'script_results/idrfpm/output.csv', 'w') as output_dat:
    output_dat.write('variable,value\n')   
  
  checkpoint = []
  checkpoint.append(time.time())
  ##################
  # Script options #
  ##################
  
  #input options
  input_method = READ_INPUT
  input_data = genfromtxt(args[0],skip_header=1,delimiter=',',usecols=(1),unpack=True)
  input_data_original = genfromtxt('input_original.csv',skip_header=1,delimiter=',',usecols=(1),unpack=True)
  
  #print(args[0])
  
  if __name__ != '__main__':
    VERBOSITY=0		#VERBOSITY setting when this script is invoked by an other script
    display_drawings=0
    #display_drawings=1
    enable_litz_wire_selection = int(input_data[48])
    enable_voltage_leeway = int(input_data[49])
    enable_manual_turnsPerCoil_limit = int(input_data[43])
  else:
    VERBOSITY=1		#VERBOSITY setting when this script is executed by itself
    #VERBOSITY=0
    display_drawings=0

    #enable_litz_wire_selection = 1
    #enable_voltage_leeway = 0
    #enable_manual_turnsPerCoil_limit = 1   
    
    enable_voltage_leeway = int(input_data[49])
    enable_manual_turnsPerCoil_limit = int(input_data[43])
    enable_litz_wire_selection = int(input_data[48])
 
  #simulation options
  enable_smc_cored_coils = int(input_data[23])
  enable_sub_air_gaps = int(input_data[24])
  enable_general_calc = int(input_data[25])
  enable_eddy_losses_calc = int(input_data[26])
  enable_flux_density_harmonic_calc = int(input_data[27])
  
  
  
  #output options
  #enable_single_csv_output = int(input_data[28])
  enable_terminal_output = int(input_data[29]) 
  enable_optimization_csv_output = int(input_data[30])
  enable_show_iteration_number = int(input_data[44])
  enable_show_inputs = int(input_data[45])
  enable_current_density = int(input_data[46])
  
  ###########################
  # Read and Update iteration number #
  ###########################

  run_flags = genfromtxt(dir_parent+'script_results/main/running_flags.csv',skip_header=0,delimiter=',',usecols=(1),unpack=True)
  iteration_number = int(run_flags)

  #################################
  # Read dimensions from csv file #
  #################################
  if input_method == READ_INPUT:
    #input_data = genfromtxt(args[0],skip_header=1,delimiter=',',usecols=(1),unpack=True)

    p		= input_data[0]							#pole pairs
    kq		= input_data[1]							#coils per pole pair per phase                           [m]
    l		= input_data[5] 						#axial rotor/stator length                    [m]
    g		= input_data[6] 						#each airgap length
    hc		= input_data[7] 						#coil/winding thickness                       [m]
    kmo		= input_data[8] 						#radial outer PM width / pole pitch
    kmi		= input_data[62] 						#radial inner PM width / pole pitch
    Brem	= input_data[9] 						#remnant flux density of NdBFe N48 PMs        [T]
    muR		= input_data[10] 						#recoil permeability of the PMs
  
    #f_e		= input_data[11]						#electrical excitation frequency	      [Hz]

    active_parts= int(input_data[19])
    turnsPerCoil= int(input_data[20])
    fill_factor = input_data[21]						#according to Gert's calculations, but we are not sure what value Maxwell actually used (2015/05/27)
    
    J		= input_data[31]*sqrt(2)					#input current density [A/mm^2] (RMS value is given by input file, thus multiply by sqrt(2)
    kc		= input_data[32]						#coil winding ratio range(0-1)
    hmo_r	= input_data[36]						#outer magnet width
    hmi_r	= input_data[37]						#inner magnet width
    ri		= input_data[38]						#most inner radius point on machine
    ro		= input_data[39]						#most outer radius point on machine

    
    go		= input_data[53]
    gi		= input_data[54]
    x_ri	= input_data[55]
    x_hmo_r	= input_data[56]
    #x_go	= input_data[57]
    x_hc	= input_data[58]
    #x_gi	= input_data[59]
    x_hmi_r	= input_data[60]
    n_rpm	= input_data[61]
    f_e		= (n_rpm*p*2)/120
    
    if enable_eddy_losses_calc == 1:
      print "Note that f_e = 70Hz for eddy current calculations. And that source current is zero because we want to simulate open circuit conditions."
      f_e = 70
      n_rpm = 300
      J = 0
    
    x_hmo_rt	= input_data[63]
    x_hmi_rt	= input_data[64]
    
    
    variable_no = input_data[52]
    perturb_count = zeros(int(variable_no))
    mesh_scale = input_data[47]

  if enable_show_inputs == 1:
    extra_input_info = genfromtxt(args[0],skip_header=1,delimiter=',',usecols=(2,3),unpack=True)
    ri_min  =  extra_input_info[1][38]
    hmo_r_min =  extra_input_info[1][36]
    go_min  =  extra_input_info[1][53]
    hc_min  =  extra_input_info[1][7]
    gi_min  =  extra_input_info[1][54]
    hmi_r_min =  extra_input_info[1][37]
  

    ###DEBUGGING
    if enable_variable_test == True:
      if __name__ != '__main__':	#when this script is invoked by an other script
	print("hallo daar")
	#these variable values are decided by main.py
	g	= input_data[6] 						#each airgap length
	hc	= input_data[7] 						#magnet thickness   
	hmo_r	= input_data[36]						#outer magnet width
	hmi_r	= input_data[37]						#inner magnet width
	ri	= input_data[38]						#most inner radius point on machine
      else:				#when this script is executed by itself

	ri  = (ro - ri_min - hmo_r_min - go_min - hc_min - gi_min - hmi_r_min)*x_ri  + ri_min
	hmo_r = (ro - ri     - hmo_r_min - go_min - hc_min - gi_min - hmi_r_min)*x_hmo_r + hmo_r_min
	go  = (ro - ri     - hmo_r     - go_min - hc_min - gi_min - hmi_r_min)*x_go  + go_min
	hc  = (ro - ri     - hmo_r     - go     - hc_min - gi_min - hmi_r_min)*x_hc  + hc_min
	gi  = (ro - ri     - hmo_r     - go     - hc     - gi_min - hmi_r_min)*x_gi  + gi_min
	hmi_r = (ro - ri     - hmo_r     - go     - hc     - gi     - hmi_r_min)*x_hmi_r + hmi_r_min
	
	print go
	
    ###
    
    if active_parts == WINDINGS_ONLY or active_parts == ALL_ACTIVE:
      #id 	= input_data[12]
      #iq 	= input_data[13]
      #these current variables will be determined by current density J
      id = 0
      iq = 0
    else:
      id = 0
      iq = 0       
 
    steps 		 = int(input_data[22])
    peak_voltage_allowed = (input_data[33]*0.95*1.15)/sqrt(3)			#The input file refers to a peak Line-to-Line (LL) voltage, 0.95 takes into account voltage source drop, 1.15 takes into account THD of the sinusoidal voltage source
    
    coil_temperature	 = input_data[34]
    
    
    if enable_optimization_csv_output == 1 and iteration_number == 0:
      with open(dir_parent+'script_results/idrfpm/optimization_output.csv', 'w') as opt_output_file:
	opt_output_file.write('p,kq,l,g,hmi_r,hc,hmo_r,kmo,kc,ri,ro,number_of_layers_eddy,wire_diameter,parallel_stranded_conductors,turnsPerCoil,fill_factor,J,max_input_phase_current,max_induced_phase_voltage,max_input_phase_voltage,average_torque_dq,magnet_mass,copper_mass,Ra_analytical,efficiency,S,Q,P,P_out,P_conductive_analytical,P_eddy,P_wf,P_core_loss,Total_losses,power_factor,coil_side_area,max_Br_1,Br1,Br_THD*100\n')

    number_of_layers_eddy        = int(input_data[14])
    max_harmonics_eddy           = int(input_data[15])
    wire_diameter                = input_data[16]
    parallel_stranded_conductors = int(input_data[17])
    wire_resistivity             = input_data[18]


  #############################
  # Enter dimensions manually #
  #############################
  elif input_method == MANUAL_INPUT:
    p = 14						#pole pairs
    kq = 0.5						#coils per pole pair per phase
    rn=120E-3 						#nominal radius                               [m]
    hy= 50E-3 						#dummy air cored yoke thickness               [m]
    hm=  6E-3 						#magnet thickness                             [m]
    l=  40E-3 						#axial rotor/stator length                    [m]
    g=   1E-3 						#each airgap length
    hc=   9E-3 						#coil/winding thickness                       [m]

    km=   0.5 						#radial PM width / pole pitch (describes the radial facing PM's width as a fraction of the width of the axial facing PMs)
    Brem= 1.4 						#remnant flux density of NdBFe N48 PMs        [T]
    muR= 1.05 						#recoil permeability of the PMs
  
    f_e = 67.667
    

    turnsPerCoil = 22
    fill_factor = 0.5 					#according to Gert's calculations, but we are not sure what value Maxwell actually used (2015/05/27)
    active_parts == ALL_ACTIVE

    if active_parts == WINDINGS_ONLY or active_parts == ALL_ACTIVE:
      id = 0
      iq = 20
    else:
      id = 0
      iq = 0

    if active_parts == MAGNETS_ONLY:
      steps = 1
    else:
      steps = int(input_data[22])   
    
    if enable_eddy_losses_calc == 1:
      number_of_layers_eddy        = 5
      max_harmonics_eddy           = 15
      wire_diameter                = 0.0016
      parallel_stranded_conductors = 1
      wire_resistivity             = 1.678e-8
    
    #simulation options
    enable_smc_cored_coils = 0
    enable_sub_air_gaps = 1
    enable_general_calc = 0
    enable_eddy_losses_calc = 0
    enable_flux_density_harmonic_calc = 0

    #output options
    enable_csv_output = 1
    enable_terminal_output = 0
  
  #############################################
  # Calculate remaining dimensions and ratios #
  #############################################
 
  #x_hmo_rt --> gives ratio between hight of outer radially magnetized PMs compared to outer tangentially magnetized PMs
  hmo_t = x_hmo_rt*hmo_r
  #x_hmi_rt --> gives ratio between hight of inner radially magnetized PMs compared to inner tangentially magnetized PMs
  hmi_t = x_hmi_rt*hmi_r
  
 
  #convert these parameters to my script's parameters
  magnetTotal = 4*p
  innerMagnetRadiusA = ri
  innerMagnetRadiusB = ri + hmi_r
  outerMagnetRadiusA = ri + hmi_r + gi + hc + go
  outerMagnetRadiusB = ro

  CoilRadiusA 	= ri + hmi_r + gi
  CoilRadiusB 	= ri + hmi_r + gi + hc

  
  innerBlockRadius = CoilRadiusA				#assuming the coil resin has no thickness
  outerBlockRadius = CoilRadiusB				#assuming the coil resin has no thickness
  coilBlockWidthRadial = outerBlockRadius - innerBlockRadius
  stack_length = l
  Br = Brem
  mur = muR
  coilsTotal = int(kq * 3 * p)


  magnetPitch = (2*pi)/magnetTotal
  axial_outer_PM_pitch = (1-kmo)*2*magnetPitch
  axial_inner_PM_pitch = (1-kmi)*2*magnetPitch
  radial_outer_PM_pitch = kmo*2*magnetPitch
  radial_inner_PM_pitch = kmi*2*magnetPitch
  angle_inc = 0.5*magnetPitch

  innerYoke = 0.9*ri
  outerYoke = 1.1*ro

  magnetPitch = (2*pi)/magnetTotal
  poles = int(magnetTotal/2)					#number of poles in rotor1, and number of poles in rotor2
  mechRotation = (4*pi)/poles					#only rotate one electrical full period (amount of radians the machine should mechanically rotate) ##mechRotation = 2*pi * (2/poles)
  rotor_symmetry_factor = int(4*magnetTotal/poles)
  symmetry_angle = (magnetTotal/(poles/4))*magnetPitch
  rotorDAxisOffset = 1.5*magnetPitch				#this angle is used to move rotor d-axis inline with the x-axis (assuming the transformation angle = 0 because we assume the phase-A axis also to be there)
  coilBlockPitch = (2*pi)/coilsTotal
  meanBlockRadius = innerBlockRadius + 0.5*coilBlockWidthRadial

  
  coil_side_radians = 0.5*kc*coilBlockPitch
  blockAngle = (1-kc)*coilBlockPitch
  coil_side_area = (CoilRadiusB**2 - CoilRadiusA**2)*(coil_side_radians/2)	#[m^2]
  single_turn_area = parallel_stranded_conductors*pi*((wire_diameter/2)**2)
  
  if enable_manual_turnsPerCoil_limit == 1:
    turnsPerCoil = int(input_data[20])
    
  if enable_current_density == True:
    current_through_slot = J*(1000**2)*coil_side_area*fill_factor			#[A]
    turnsPerCoil = 1
    iq = current_through_slot/turnsPerCoil					#[A]
  else:
    current_through_slot = iq*turnsPerCoil
  
  windingRadians = coil_side_radians
  
  N_L = 2							#Number of layer stack windings (if N_L == 1, then an error is given)
  N_S = 1							#Overlap number. This is a non-overlapping coil structure, thus N_S = 1
  phase = [1,-1,3,-3,2,-2]

  end_winding_radius = ((blockAngle+windingRadians)*meanBlockRadius)/2

  #Air-Gaps
  innerGap = innerBlockRadius - innerMagnetRadiusB
  outerGap = outerMagnetRadiusA - outerBlockRadius

  mesh_multiplier1 = (ro-ri)/(0.131-0.109)
  mesh_multiplier2 = 14/p

  #mesh_multiplier1 = (input_data_original[39]-input_data_original[38])/(ro-ri)
  #mesh_multiplier2 = input_data_original[0]/p
  
  #print "NEW MACHINE"
  #print (mesh_multiplier1)
  #print (mesh_multiplier2)
  
  #mesh_scale = int(mesh_scale*mesh_multiplier1*mesh_multiplier2)
  
  #if mesh_scale <= 1: mesh_scale=1

  mesh_scale = mesh_scale*mesh_multiplier1*mesh_multiplier2
  #print (mesh_scale)
  if mesh_scale <1: mesh_scale=1

  #Mesh size settings
  #magnet_mesh = 2e-3
  magnet_mesh = 1e-3*mesh_scale
  #gmesh = 2e-3
  gmesh = 1e-3*mesh_scale	#temporary value for csv Br exportation

  #Arc length settings
  arc_len = 5e-4*mesh_scale
  gap_len = 0.5e-3*mesh_scale					#Stiaan added this parameter
  boundary_len = 1e-3*mesh_scale


  if enable_show_inputs == 1:
    pretty_input_table = PrettyTable(['SEMFEM Input Variable','Perturb Count','Minimum Value','Maximum Value','Initial Value','Present Value','Percentage Change','Unit'])
    pretty_input_table.align = 'l'
    pretty_input_table.border= True
    extra_input_info = genfromtxt(args[0],skip_header=1,delimiter=',',usecols=(2,3),unpack=True)

    if iteration_number == 0 or __name__ == '__main__':
      for i in range(0,size(perturb_count)):
	perturb_count[i] = 0
    else:
      perturb_count = genfromtxt(dir_parent+'script_results/main/perturb_count.csv',skip_header=1,usecols=(1),delimiter=",",unpack=True)
      #perturb_count = np.loadtxt('./script_results/main/perturb_count.csv',skip_header=1,usecols=(1),delimiter=",",unpack=True)
 
    pretty_input_table.add_row(["ABSOLUTE VALUES","","","","","","",""])
    pretty_input_table.add_row(["ro",0,extra_input_info[1][39],extra_input_info[0][39],input_data_original[39],ro,"%.3f"%(((ro-input_data_original[39])/input_data_original[39])*100),"[m]"])
    pretty_input_table.add_row(["ri",int(perturb_count[0]),extra_input_info[1][38],extra_input_info[0][38],input_data_original[38],ri,"%.3f"%(((ri-input_data_original[38])/input_data_original[38])*100),"[m]"])
    pretty_input_table.add_row(["hmo_r",int(perturb_count[1]),extra_input_info[1][36],extra_input_info[0][36],input_data_original[36],hmo_r,"%.3f"%(((hmo_r-input_data_original[36])/input_data_original[36])*100),"[m]"])
    pretty_input_table.add_row(["hc",int(perturb_count[2]),extra_input_info[1][7],extra_input_info[0][7],input_data_original[7],hc,"%.3f"%(((hc-input_data_original[7])/input_data_original[7])*100),"[m]"])
    pretty_input_table.add_row(["hmi_r",int(perturb_count[3]),extra_input_info[1][37],extra_input_info[0][37],input_data_original[37],hmi_r,"%.3f"%(((hmi_r-input_data_original[37])/input_data_original[37])*100),"[m]"])    
    pretty_input_table.add_row(["g",0,extra_input_info[1][6],extra_input_info[0][6],input_data_original[6],g,"%.3f"%(((g-input_data_original[6])/input_data_original[6])*100),"[m]"])    
    pretty_input_table.add_row(["l",0,extra_input_info[1][5],extra_input_info[0][5],input_data_original[5],l,"%.3f"%(((l-input_data_original[5])/input_data_original[5])*100),"[m]"])        
    pretty_input_table.add_row(["kmo",int(perturb_count[4]),extra_input_info[1][8],extra_input_info[0][8],input_data_original[8],kmo,"%.3f"%(((kmo-input_data_original[8])/input_data_original[8])*100),"[p.u.]"])
    pretty_input_table.add_row(["kc",int(perturb_count[5]),extra_input_info[1][32],extra_input_info[0][32],input_data_original[32],kc,"%.3f"%(((kc-input_data_original[32])/input_data_original[32])*100),"[p.u.]"])
    pretty_input_table.add_row(["p",int(perturb_count[6]),extra_input_info[1][0],extra_input_info[0][0],input_data_original[0],p,"%.3f"%(((p-input_data_original[0])/input_data_original[0])*100),"[p.u.]"])
    #pretty_input_table.add_row(["kmi",int(perturb_count[7]),extra_input_info[1][62],extra_input_info[0][62],input_data_original[62],kmi,"%.3f"%(((kmi-input_data_original[62])/input_data_original[62])*100),"[p.u.]"])     
    #pretty_input_table.add_row(["go",int(perturb_count[10]),extra_input_info[1][53],extra_input_info[0][53],input_data_original[53],go,"%.3f"%(((go-input_data_original[53])/input_data_original[53])*100),"[m]"])
    #pretty_input_table.add_row(["gi",int(perturb_count[11]),extra_input_info[1][54],extra_input_info[0][54],input_data_original[54],gi,"%.3f"%(((gi-input_data_original[54])/input_data_original[54])*100),"[m]"])
    
    pretty_input_table.add_row(["RELATIVE VALUES","","","","","","",""])
    pretty_input_table.add_row(["x_ri" ,int(perturb_count[0]),extra_input_info[1][55],extra_input_info[0][55],input_data_original[55],x_ri,"%.3f"%(((x_ri-input_data_original[55])/input_data_original[55])*100),"[p.u.]"])
    pretty_input_table.add_row(["x_hmo_r",int(perturb_count[1]),extra_input_info[1][56],extra_input_info[0][56],input_data_original[56],x_hmo_r,"%.3f"%(((x_hmo_r-input_data_original[56])/input_data_original[56])*100),"[p.u.]"])
    #pretty_input_table.add_row(["x_go" ,int(perturb_count[10]),extra_input_info[1][57],extra_input_info[0][57],input_data_original[57],x_go,"%.3f"%(((x_go-input_data_original[57])/input_data_original[57])*100),"[p.u.]"])
    pretty_input_table.add_row(["x_hc" ,int(perturb_count[2]),extra_input_info[1][58],extra_input_info[0][58],input_data_original[58],x_hc,"%.3f"%(((x_hc-input_data_original[58])/input_data_original[58])*100),"[p.u.]"])
    #pretty_input_table.add_row(["x_gi" ,int(perturb_count[11]),extra_input_info[1][59],extra_input_info[0][59],input_data_original[59],x_gi,"%.3f"%(((x_gi-input_data_original[59])/input_data_original[59])*100),"[p.u.]"])
    pretty_input_table.add_row(["x_hmi_r" ,int(perturb_count[3]),extra_input_info[1][60],extra_input_info[0][60],input_data_original[60],x_hmi_r,"%.3f"%(((x_hmi_r-input_data_original[60])/input_data_original[60])*100),"[p.u.]"])
    #pretty_input_table.add_row(["x_hmo_rt",int(perturb_count[8]),extra_input_info[1][63],extra_input_info[0][63],input_data_original[63],x_hmo_rt,"%.3f"%(x_hmo_rt*100),"[p.u.]"]) 
    #pretty_input_table.add_row(["x_hmi_rt",int(perturb_count[9]),extra_input_info[1][64],extra_input_info[0][64],input_data_original[64],x_hmi_rt,"%.3f"%(x_hmi_rt*100),"[p.u.]"])
    
    save_stdout = sys.stdout
    sys.stdout = open(dir_parent+"pretty_input_table.txt", "wb")
    print pretty_input_table
    sys.stdout = save_stdout    

    #if enable_terminal_output == 1:
      #print pretty_input_table

  #########################
  # SEMFEM Setup settings #
  #########################

  sf = SEMFEM()	#initialize object

  sf.semfem_begin(dir_parent+"project_idrfpm_semfem")
  sf.semfem_set_problem_steps(steps)

  #air gaps between inner rotor and coil, and outer rotor and coil
  sf.semfem_set_air_gap_method(2)			#band solver (was used) (2 denotes 2 airgaps!!)
  sf.semfem_set_problem_type(sf.ROTATING)
  sf.semfem_current_input_method = sf.PHASE_CURRENT
  #sf.semfem_current_input_method = sf.PHASE_CURRENT_DENSITY
  sf.semfem_set_stack_length(stack_length)
  sf.semfem_set_bh_curve("/usr/share/SEMFEM/data/Laminations/Magnet_M-19_29_Ga.bh")	#apparantly this is the default magnet BH curve data
  sf.semfem_set_verbosity(VERBOSITY)			#no print-outs

  ####################
  # Drawing commands #
  ####################

  # Inner magnets
  ###############
  if active_parts != MAGNETS_ONLY and steps != 1:
    inner_magnet_arc_len_A = (innerMagnetRadiusA*mechRotation)/(steps-1)
    inner_magnet_arc_len_B = (innerMagnetRadiusB*mechRotation)/(steps-1)
  else:
    inner_magnet_arc_len_A = arc_len
    inner_magnet_arc_len_B = arc_len

  #print "whole_number calc"
  #print(innerMagnetRadiusB)
  #print(innerMagnetRadiusA)
  #print(boundary_len)
  #print((innerMagnetRadiusB-innerMagnetRadiusA)/boundary_len)


  
  pitstop = innerMagnetRadiusA+((1-x_hmi_rt)*hmi_r)
  whole2 = (pitstop-innerMagnetRadiusA)/magnet_mesh
  if whole2 < 1:
    whole2 = 1
  else:
    whole2 = int(round(whole2))
  
  #whole_number = int((innerMagnetRadiusB-innerMagnetRadiusA)/boundary_len)
  whole_number = (innerMagnetRadiusB-pitstop)/boundary_len
  #print(whole_number)
  if whole_number < 1:
    whole_number = 1
  else:
    whole_number = int(round(whole_number))

  sf.pnline(innerMagnetRadiusA, 0,pitstop, 0, whole2)
  sf.pnline(pitstop, 0,innerMagnetRadiusB, 0, whole_number)
  #sf.pmline(innerMagnetRadiusA, 0,innerMagnetRadiusB, 0, boundary_len)
  
  ###tangential facing PMs###
  sf.begin_part()    
  sf.oarc(0, axial_inner_PM_pitch, innerMagnetRadiusA, inner_magnet_arc_len_A)		#arc length would not matter to much here 
  sf.oarc(0, axial_inner_PM_pitch, pitstop, inner_magnet_arc_len_B) 			#this has to be correct arc length
  sf.oarc(0, axial_inner_PM_pitch, innerMagnetRadiusB, inner_magnet_arc_len_B) 		#this has to be correct arc length
  sf.pline(innerMagnetRadiusA, axial_inner_PM_pitch,pitstop, axial_inner_PM_pitch)
  sf.pline(pitstop, axial_inner_PM_pitch,innerMagnetRadiusB, axial_inner_PM_pitch)

  #make copies of the tangential facing PM 
  sf.nrotate(0, 0, 2*magnetPitch, int((rotor_symmetry_factor - 2)/2))
  sf.end_part()

  ###radial facing PMs###
  sf.begin_part()    
  sf.oarc(axial_inner_PM_pitch, axial_inner_PM_pitch + radial_inner_PM_pitch, innerMagnetRadiusA, inner_magnet_arc_len_A)		#arc length would not matter to much here 
  sf.oarc(axial_inner_PM_pitch, axial_inner_PM_pitch + radial_inner_PM_pitch, innerMagnetRadiusB, inner_magnet_arc_len_B) 	#this has to be correct arc length
  sf.pline(innerMagnetRadiusA, axial_inner_PM_pitch + radial_inner_PM_pitch,pitstop, axial_inner_PM_pitch + radial_inner_PM_pitch)
  sf.pline(pitstop, axial_inner_PM_pitch + radial_inner_PM_pitch,innerMagnetRadiusB, axial_inner_PM_pitch + radial_inner_PM_pitch)

  #make copies of the radial facing PM 
  sf.nrotate(0, 0, 2*magnetPitch, int((rotor_symmetry_factor - 2)/2) -1)
  sf.end_part()
  
  ###
  sf.oarc((rotor_symmetry_factor*magnetPitch) - radial_inner_PM_pitch, rotor_symmetry_factor*magnetPitch, innerMagnetRadiusA, inner_magnet_arc_len_A)	#arc length would not matter to much here 
  sf.oarc((rotor_symmetry_factor*magnetPitch) - radial_inner_PM_pitch, rotor_symmetry_factor*magnetPitch, innerMagnetRadiusB, inner_magnet_arc_len_B) 	#this has to be correct arc length
  
  sf.pnline(innerMagnetRadiusA, symmetry_angle,pitstop, symmetry_angle, whole2)
  sf.pnline(pitstop, symmetry_angle,innerMagnetRadiusB, symmetry_angle, whole_number)
  #sf.pmline(innerMagnetRadiusA, symmetry_angle,innerMagnetRadiusB, symmetry_angle, boundary_len)  
  ###
  
  #give polarity for each inner magnet
  angle = 0
  orientation = 1
  inner_magnet_centre_point_r = 0.5*(innerMagnetRadiusA+innerMagnetRadiusB)
  inner_magnet_centre_point_t = 0.5*(innerMagnetRadiusA+pitstop)
  inner_magnet_centre_point_t_air = 0.5*(pitstop+innerMagnetRadiusB)

  for i in range (rotor_symmetry_factor): #orientate inner magnets in Halbach formation
    
    if orientation == 1:	#tangential clockwise direction (yellow)
      if x_hmi_rt != 1:
	sf.cpmagnet(inner_magnet_centre_point_t, angle + 0.5*axial_inner_PM_pitch, "rt", 0, Br, mur, magnet_mesh, 1, 1, 0)
      if x_hmi_rt != 0:
	sf.pregion(inner_magnet_centre_point_t_air, angle + 0.5*axial_inner_PM_pitch, 0, gmesh)				#air-cored coil

    elif orientation == 2:	#radially outward direction (red)
      sf.cpmagnet(inner_magnet_centre_point_r, angle + 0.5*radial_inner_PM_pitch, "rt", Br, 0, mur, magnet_mesh, 1, 0, 0)
      
    elif orientation == 3:	#tangential anti-clockwise direction (light blue)
      if x_hmi_rt != 1:
	sf.cpmagnet(inner_magnet_centre_point_t, angle + 0.5*axial_inner_PM_pitch, "rt", 0, -Br, mur, magnet_mesh, 0, 1, 1)
      if x_hmi_rt != 0:
	sf.pregion(inner_magnet_centre_point_t_air, angle + 0.5*axial_inner_PM_pitch, 0, gmesh)
  
    else:			#radially inward direction (blue)
      sf.cpmagnet(inner_magnet_centre_point_r, angle + 0.5*radial_inner_PM_pitch, "rt", -Br, 0, mur, magnet_mesh, 0, 0, 1)
      
    if orientation%2 == 1:
      angle = angle + axial_inner_PM_pitch
    else:
      angle = angle + radial_inner_PM_pitch
    
    orientation = orientation + 1 
    if orientation > 4:
      orientation = 1


  # Outer magnets
  ###############
  if active_parts != MAGNETS_ONLY and steps != 1:
    outer_magnet_arc_len_A = (outerMagnetRadiusA*mechRotation)/(steps-1)
    outer_magnet_arc_len_B = (outerMagnetRadiusB*mechRotation)/(steps-1)
  else:
    outer_magnet_arc_len_A = arc_len
    outer_magnet_arc_len_B = arc_len


  test1 = innerMagnetRadiusA + hmi_r + gi + hc + go
  test4 = outerMagnetRadiusB - test1
  #test2 = outerMagnetRadiusB - hmo_r
  #test3 = test2-test1
  #print("test1 = %f"%(test1))
  #print("test2 = %f"%(test2))
  #print("test3 = %f"%(test3))
  #print("test4 = %f"%(test4))
  #print("x_hmo_rt = %f"%(x_hmo_rt))
  #print("hmo_r = %f"%(hmo_r))


  #pitstop = innerMagnetRadiusA + hmi_r + gi + hc + go + x_hmo_rt*hmo_r
  #pitstop = outerMagnetRadiusB-((1-x_hmo_rt)*hmo_r)
  #pitstop = test1 + 0.5*test3 + x_hmo_rt*hmo_r
  pitstop = test1 + x_hmo_rt*test4
  
  #print hmo_r
  #print x_hmo_rt
  #print pitstop
  whole2 = (pitstop-outerMagnetRadiusA)/magnet_mesh
  if whole2 < 1:
    whole2 = 1
  else:
    whole2 = int(round(whole2))
  
  whole_number = int(round((outerMagnetRadiusB-pitstop)/boundary_len))
  if whole_number <= 0: whole_number = 1
  
  #print outerMagnetRadiusA
  #print outerMagnetRadiusB
  #print pitstop
  #print whole2
  #print magnet_mesh
  
  sf.pnline(outerMagnetRadiusA, 0,pitstop, 0, whole2)
  sf.pnline(pitstop, 0,outerMagnetRadiusB, 0, whole_number)
  #sf.pmline(outerMagnetRadiusA, 0,outerMagnetRadiusB, 0, boundary_len)
  
  ###tangential facing PMs###
  sf.begin_part()
  sf.oarc(0, axial_outer_PM_pitch, pitstop, outer_magnet_arc_len_A) #this has to be correct arc length
  sf.oarc(0, axial_outer_PM_pitch, outerMagnetRadiusB, outer_magnet_arc_len_B) #arc length would not matter to much here
  sf.oarc(0, axial_outer_PM_pitch, outerMagnetRadiusA, outer_magnet_arc_len_A) #arc length would not matter to much here
  sf.pline(outerMagnetRadiusA, axial_outer_PM_pitch,pitstop, axial_outer_PM_pitch)
  sf.pline(pitstop, axial_outer_PM_pitch,outerMagnetRadiusB, axial_outer_PM_pitch)
  
  #make copies of the tangential facing PM 
  sf.nrotate(0, 0, 2*magnetPitch, int((rotor_symmetry_factor - 2)/2))
  sf.end_part()

  ###radial facing PMs###
  sf.begin_part()
  sf.oarc(axial_outer_PM_pitch, axial_outer_PM_pitch + radial_outer_PM_pitch, outerMagnetRadiusA, outer_magnet_arc_len_A) #this has to be correct arc length
  sf.oarc(axial_outer_PM_pitch, axial_outer_PM_pitch + radial_outer_PM_pitch, outerMagnetRadiusB, outer_magnet_arc_len_B) #arc length would not matter to much here
  sf.pline(outerMagnetRadiusA, axial_outer_PM_pitch + radial_outer_PM_pitch,pitstop, axial_outer_PM_pitch + radial_outer_PM_pitch)
  sf.pline(pitstop, axial_outer_PM_pitch + radial_outer_PM_pitch,outerMagnetRadiusB, axial_outer_PM_pitch + radial_outer_PM_pitch)
  
  #make copies of the radial facing PM 
  sf.nrotate(0, 0, 2*magnetPitch, int((rotor_symmetry_factor - 2)/2)-1)
  sf.end_part()
  
  ###
  sf.oarc((rotor_symmetry_factor*magnetPitch) - radial_outer_PM_pitch, rotor_symmetry_factor*magnetPitch, outerMagnetRadiusA, outer_magnet_arc_len_A) #this has to be correct arc length
  sf.oarc((rotor_symmetry_factor*magnetPitch) - radial_outer_PM_pitch, rotor_symmetry_factor*magnetPitch, outerMagnetRadiusB, outer_magnet_arc_len_B) #arc length would not matter to much here
  sf.pnline(outerMagnetRadiusA, symmetry_angle,pitstop, symmetry_angle, whole2)
  sf.pnline(pitstop, symmetry_angle,outerMagnetRadiusB, symmetry_angle, whole_number)
  #sf.pmline(outerMagnetRadiusA, symmetry_angle,outerMagnetRadiusB, symmetry_angle, boundary_len)
  ###

  angle = 0
  orientation = 1
  outer_magnet_centre_point_r = 0.5*(outerMagnetRadiusA+outerMagnetRadiusB)
  outer_magnet_centre_point_t = 0.5*(outerMagnetRadiusB+pitstop)
  outer_magnet_centre_point_t_air = 0.5*(outerMagnetRadiusA+pitstop)


  for i in range (rotor_symmetry_factor):  
    
    if orientation == 1:	#tangetial anti-clockwise direction (yellow)
	if x_hmo_rt != 1:
	  sf.cpmagnet(outer_magnet_centre_point_t, angle + 0.5*axial_outer_PM_pitch, "rt", 0, -Br, mur, magnet_mesh, 0, 1, 1)
	if x_hmo_rt != 0:
	  sf.pregion(outer_magnet_centre_point_t_air, angle + 0.5*axial_inner_PM_pitch, 0, gmesh)

    elif orientation == 2:	#radially outward direction (red)
	sf.cpmagnet(outer_magnet_centre_point_r, angle + 0.5*radial_outer_PM_pitch, "rt", Br, 0, mur, magnet_mesh, 1, 0, 0)
	
    elif orientation == 3:	#tangential clockwise direction  (light blue)
	if x_hmo_rt != 1:
	  sf.cpmagnet(outer_magnet_centre_point_t, angle + 0.5*axial_outer_PM_pitch, "rt", 0, Br, mur, magnet_mesh, 1, 1, 0)
	if x_hmo_rt != 0:
	  sf.pregion(outer_magnet_centre_point_t_air, angle + 0.5*axial_inner_PM_pitch, 0, gmesh)
    
    else:			#radially inward direction (blue)
	sf.cpmagnet(outer_magnet_centre_point_r, angle + 0.5*radial_outer_PM_pitch, "rt", -Br, 0, mur, magnet_mesh, 0, 0, 1)
      
    if orientation%2 == 1:
      angle = angle + axial_outer_PM_pitch
    else:
      angle = angle + radial_outer_PM_pitch

    orientation = orientation + 1 
    if orientation > 4:
      orientation = 1


  # Stator coil cores (blocks) and windings
  #########################################
  sf.begin_part()
  angle_temp1 = windingRadians + blockAngle
  angle_temp2 = int(coilsTotal/(coilsTotal/3)-1)*coilBlockPitch
  angle_temp3 = angle_temp2 + angle_temp1
  
  if active_parts != MAGNETS_ONLY and steps != 1:
    stator_arc_len_inner = (innerBlockRadius*mechRotation)/(steps-1)
    stator_arc_len_outer = (outerBlockRadius*mechRotation)/(steps-1)
  else:
    stator_arc_len_inner = arc_len
    stator_arc_len_outer = arc_len  

  sf.oarc(0, windingRadians, innerBlockRadius, stator_arc_len_inner)		#coil block inside arc
  sf.oarc(0, windingRadians, outerBlockRadius, stator_arc_len_outer)		#coil block inside arc
  sf.pline(innerBlockRadius, windingRadians, outerBlockRadius, windingRadians)

  sf.oarc(windingRadians, angle_temp1, innerBlockRadius, stator_arc_len_inner)	#coil block inside arc
  sf.oarc(windingRadians, angle_temp1, outerBlockRadius, stator_arc_len_outer)	#coil block inside arc
  sf.pline(innerBlockRadius, angle_temp1, outerBlockRadius, angle_temp1)

  sf.oarc( angle_temp1, coilBlockPitch, innerBlockRadius, stator_arc_len_inner)	#coil block inside arc
  sf.oarc( angle_temp1, coilBlockPitch, outerBlockRadius, stator_arc_len_outer)	#coil block inside arc
  sf.pline(innerBlockRadius, coilBlockPitch, outerBlockRadius, coilBlockPitch)

  ###
  sf.nrotate(0, 0, coilBlockPitch, int(coilsTotal/(coilsTotal/3)-2))

  sf.oarc(angle_temp2, angle_temp2 + windingRadians, innerBlockRadius, stator_arc_len_inner)	#coil block inside arc
  sf.oarc(angle_temp2, angle_temp2 + windingRadians, outerBlockRadius, stator_arc_len_outer)	#coil block inside arc
  sf.pline(innerBlockRadius, angle_temp2 + windingRadians, outerBlockRadius, angle_temp2 + windingRadians)

  sf.oarc(angle_temp2 + windingRadians, angle_temp3, innerBlockRadius, stator_arc_len_inner)	#coil block inside arc
  sf.oarc(angle_temp2 + windingRadians, angle_temp3, outerBlockRadius, stator_arc_len_outer)	#coil block inside arc
  sf.pline(innerBlockRadius, angle_temp3, outerBlockRadius, angle_temp3)

  sf.oarc(angle_temp3, angle_temp2 + coilBlockPitch, innerBlockRadius, stator_arc_len_inner)	#coil block inside arc
  sf.oarc(angle_temp3, angle_temp2 + coilBlockPitch, outerBlockRadius, stator_arc_len_outer)	#coil block inside arc
  
  whole_number = int((outerBlockRadius-innerBlockRadius)/boundary_len)
  if whole_number <= 0: whole_number = 1
  sf.pnline(innerBlockRadius, angle_temp2 + coilBlockPitch, outerBlockRadius, angle_temp2 + coilBlockPitch, whole_number)
  sf.pnline(innerBlockRadius, 0, outerBlockRadius, 0, whole_number)
  
  
  ###

  #assign stator coil windings 
  winding = zeros((coilsTotal,2))
  phase_A_offset = sf.assign_winding2(3, int(poles/2), coilsTotal, N_L, N_S, winding)			#This gives the correct answer as it correlates with fpl viewer's theta (t) reading

  #declare coils and specify which windings they belong to
  present_phase = 0
  angle = 0.5*windingRadians

  for i in range (int(coilsTotal/(coilsTotal/3)*2)):  
    if(present_phase > 5):
      present_phase = 0
    sf.pregion(0.5*(innerBlockRadius+outerBlockRadius), angle, phase[present_phase], gmesh)		#coils has same permeability as air
    if(i%2 == 0):
      angle = angle + angle_temp1
    else:
      angle = angle + windingRadians
      
    present_phase = present_phase + 1

  angle = windingRadians + 0.5*blockAngle

  for i in range (int(coilsTotal/(coilsTotal/3))):
    if enable_smc_cored_coils == 0:
      sf.pregion(0.5*(innerBlockRadius+outerBlockRadius), angle, 0, gmesh)				#air-cored coil
    else:
      #SMC material is dummied by creating a magnet region with Br=0, but with mur = 850
      Br = 0
      mur = 850
      sf.cpmagnet(0.5*(innerBlockRadius+outerBlockRadius),angle, "rt", Br, Br, mur, gmesh, 0.2, 0.4, 1)	#smc-cored coil
      
    angle += 2*windingRadians + blockAngle

  sf.end_part()


  #Additional drawing commands
  ############################
  # Add layers inside yoke
  insideYokeLayer  = innerMagnetRadiusA*0.98
  outsideYokeLayer = outerMagnetRadiusB*1.02
    
  if active_parts != MAGNETS_ONLY and steps != 1:
    #add yoke material (essentially the same permeability as air due to ironless design)
    #this is necessary to simulate flux paths moving through the yoke and the air outside of the yoke
    
    sf.oarc(0, 2*mechRotation, insideYokeLayer, (insideYokeLayer*mechRotation)/(steps-1))
    sf.oarc(0, 2*mechRotation, outsideYokeLayer, (outsideYokeLayer*mechRotation)/(steps-1)) 
 
    #yoke outside nodes dont have to be this fine, it just wastes computational time
    sf.oarc(0, 2*mechRotation, innerYoke, gmesh*5*(innerYoke/outerYoke))
    sf.oarc(0, 2*mechRotation, outerYoke, gmesh*5)

  else:
    sf.oarc(0, 2*mechRotation, insideYokeLayer, magnet_mesh)
    sf.oarc(0, 2*mechRotation, outsideYokeLayer, magnet_mesh) 
 
    #yoke outside nodes dont have to be this fine, it just wastes computational time
    sf.oarc(0, 2*mechRotation, innerYoke, gmesh*5*(innerYoke/outerYoke))
    sf.oarc(0, 2*mechRotation, outerYoke, gmesh*5)

  # Multilayer air-gaps
  if active_parts != MAGNETS_ONLY and steps != 1:
    sf.oarc(0, 2*mechRotation, innerMagnetRadiusB + innerGap/3, ((innerMagnetRadiusB + innerGap/3)*mechRotation)/(steps-1))
    sf.oarc(0, 2*mechRotation, innerBlockRadius - innerGap/3, ((innerBlockRadius - innerGap/3)*mechRotation)/(steps-1))
    sf.oarc(0, 2*mechRotation, outerBlockRadius + outerGap/3, ((outerBlockRadius + outerGap/3)*mechRotation)/(steps-1))
    sf.oarc(0, 2*mechRotation, outerMagnetRadiusA - outerGap/3, ((outerMagnetRadiusA - outerGap/3)*mechRotation)/(steps-1))
  else:
    sf.oarc(0, 2*mechRotation, innerMagnetRadiusB + innerGap/3, gap_len)
    sf.oarc(0, 2*mechRotation, innerBlockRadius - innerGap/3, gap_len)
    sf.oarc(0, 2*mechRotation, outerBlockRadius + outerGap/3, gap_len)
    sf.oarc(0, 2*mechRotation, outerMagnetRadiusA - outerGap/3, gap_len)    

  # Boundary nodes
  whole_number = int((innerMagnetRadiusA-insideYokeLayer)/magnet_mesh)
  if whole_number <= 0: whole_number = 1
  sf.pnline(insideYokeLayer, 0, innerMagnetRadiusA, 0, whole_number)
  sf.pnline(insideYokeLayer, symmetry_angle, innerMagnetRadiusA, symmetry_angle, whole_number)  
  
  whole_number = int((outsideYokeLayer-outerMagnetRadiusB)/magnet_mesh)
  if whole_number <= 0: whole_number = 1
  sf.pnline(outerMagnetRadiusB, 0,outsideYokeLayer, 0, whole_number)
  sf.pnline(outerMagnetRadiusB, symmetry_angle,outsideYokeLayer, symmetry_angle, whole_number)


  whole_number = int((insideYokeLayer-innerYoke)/(gmesh*5*(innerYoke/outerYoke)))
  if whole_number <= 0: whole_number = 1
  sf.pnline(innerYoke, 0,insideYokeLayer, 0, whole_number)
  sf.pnline(innerYoke, symmetry_angle, insideYokeLayer, symmetry_angle, whole_number)
  
  whole_number = int((outerYoke-outsideYokeLayer)/(gmesh*5))
  if whole_number <= 0: whole_number = 1
  sf.pnline(outsideYokeLayer, 0,outerYoke, 0, whole_number)
  sf.pnline(outsideYokeLayer, symmetry_angle,outerYoke, symmetry_angle, whole_number)

  #draw lines to close sub-air-gap sections (also boundary nodes)
  if (enable_sub_air_gaps == 1):
    sf.pline(innerMagnetRadiusB, 0, innerMagnetRadiusB + innerGap/3, 0)
    sf.pline(innerBlockRadius - innerGap/3, 0, innerBlockRadius, 0)
    sf.pline(outerBlockRadius, 0, outerBlockRadius + outerGap/3, 0)
    sf.pline(outerMagnetRadiusA - outerGap/3, 0, outerMagnetRadiusA, 0)

    sf.pline(innerMagnetRadiusB, symmetry_angle, innerMagnetRadiusB + innerGap/3, symmetry_angle)
    sf.pline(innerBlockRadius - innerGap/3, symmetry_angle, innerBlockRadius, symmetry_angle)
    sf.pline(outerBlockRadius, symmetry_angle, outerBlockRadius + outerGap/3, symmetry_angle)
    sf.pline(outerMagnetRadiusA - outerGap/3, symmetry_angle, outerMagnetRadiusA, symmetry_angle)
    
  #############################
  # Final simulation settings #
  #############################
  #os.system("fpl2 project_idrfpm_semfem/drawing.poly")
  #os.system("fpl2 project_idrfpm_semfem/drawing.fpl")
  #sys.exit()

  if enable_sub_air_gaps == 1:
    sf.semfem_set_cvw_gaps(2, [innerMagnetRadiusB + innerGap/3, outerBlockRadius + outerGap/3], [innerBlockRadius - innerGap/3, outerMagnetRadiusA - outerGap/3])	#coulomb vector w.. method used to calculate torque in air gaps. this function does not effect an age_solver simulation
  else:
    sf.semfem_set_cvw_gaps(2, [innerMagnetRadiusB, outerBlockRadius], [innerBlockRadius, outerMagnetRadiusA])	#coulomb vector w.. method used to calculate torque in air gaps. this function does not effect an age_solver simulation

  ###FROM OE_MAC.py periodic example
  sf.semfem_set_left_boundary(sf.EVEN_PERIODIC)		#because this section repeats itself by an even number (on the left boundary)
  sf.semfem_set_right_boundary(sf.EVEN_PERIODIC)		#because this section repeats itself by an even number (on the right boundary)
  sf.semfem_set_top_boundary(sf.DIRICHLET)			#top section Dirichlet boundary (determines in which directions the flux can be solved)
  sf.semfem_set_bottom_boundary(sf.DIRICHLET)			#bottom section Dirichlet boundary (determines in which directions the flux can be solved)
  ###

  sf.semfem_coil_turns[:] = turnsPerCoil			#define number of turns for each coil (22) (this line not needed if using sf.PHASE_CURRENT_DENSITY)
  sf.semfem_coil_sides[:] = N_L*coilsTotal/3	   		#define number of coils for each phase (21/3)


		  
  #specify fill factor
  for i in range(size(sf.semfem_ff_vec)):			#Although it is only necessary to assign values to the first 3 elements of the array (3 phase machine) the other elements are assigned these values just for contingency.
      sf.semfem_ff_vec[i] = fill_factor
		  

  for i in range(steps):					#simulates an entire electrical period in the allocated number of steps
    #if active_parts != MAGNETS_ONLY:
    if active_parts != MAGNETS_ONLY and steps != 1:
      sf.semfem_time_vec[i] = ((i+1)/steps)/f_e			#this is necessary to setup the electrical operational frequency (f_e) for the motor
								#it also adjusts how fast (number of steps) the simulation runs through a single electrical period

    #Controls the position of different components at each step. Components are seperated by air-gaps and are numbered from the inside out.
      sf.semfem_p_vec[i,0] = (i/(steps-1))*(mechRotation) - rotorDAxisOffset + phase_A_offset		#the second array index indicates rotation of the inner rotor [0]
      sf.semfem_p_vec[i,2] = (i/(steps-1))*(mechRotation) - rotorDAxisOffset + phase_A_offset		#the second array index indicates rotation of the outer rotor [2]

      [ia, ib, ic] = [0, 0, 0]
      ia, ib, ic = sf.dq_abc(id, iq, (i*2.*pi)/(steps-1))	#3rd function argument is the transformation angle of the dq transformation
      #use the following three lines for phase current quantities
      #sf.semfem_i_vec[i,0] = ia
      #sf.semfem_i_vec[i,1] = ib
      #sf.semfem_i_vec[i,2] = ic
      #use the following three lines for phase current density quantities
      sf.semfem_idens_vec[i,0] = ia
      sf.semfem_idens_vec[i,1] = ib
      sf.semfem_idens_vec[i,2] = ic 
    
    else:
      sf.semfem_p_vec[i,0] = 0 - rotorDAxisOffset + phase_A_offset		#the second array index indicates rotation of the inner rotor [0]
      sf.semfem_p_vec[i,2] = 0 - rotorDAxisOffset + phase_A_offset		#the second array index indicates rotation of the outer rotor [2]
      
      [ia, ib, ic] = [0, 0, 0]
      #ia, ib, ic = sf.dq_abc(id, iq, (i*2.*pi)/(steps))
      ia, ib, ic = sf.dq_abc(id, iq, 0)
      #sf.semfem_i_vec[i,0] = ia
      #sf.semfem_i_vec[i,1] = ib
      #sf.semfem_i_vec[i,2] = ic      

      sf.semfem_idens_vec[i,0] = ia
      sf.semfem_idens_vec[i,1] = ib
      sf.semfem_idens_vec[i,2] = ic    

  #symmetry settings
  sf.semfem_set_symmetry_multiplier(int(poles/4))		#number of periodic occurances of rotor structure
  sf.semfem_set_parallel_circuits(int(coilsTotal/3)) 		#number of parallel circuits for each stator phase winding

  ##################    
  # Construct mesh #
  ##################
  #os.system("fpl2 mp/p1/project_idrfpm_semfem/drawing.poly")
  if display_drawings == 1:
    os.system("fpl2 project_idrfpm_semfem/drawing.poly")
  #sys.exit()
  m = sf.py_make_mesh(sf.NO_FIX)          			#previously used sf.FIX, why? Because polygon was not accurately constructed

  #just to view the drawing before actually running the solver
  #os.system("fpl2 project_idrfpm_semfem/drawing.poly")
  if display_drawings == 1:
    os.system("fpl2 project_idrfpm_semfem/drawing.fpl")
  #sys.exit()

  ##########
  # Solver #
  ##########
  #m = sf.age_solver(m, sf.ICCG)				#takes much longer to simulate
  m = sf.band_solver(m, sf.GAUSS, 1)				#was so, is dieselfde as in Bola se script

  #############################
  # View fieldplots or graphs #
  #############################
  if display_drawings == 1:
    os.system("fpl2 project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl")
  #os.system("fpl2 project_idrfpm_semfem/results/fieldplots/age_machine_field001.fpl") 
  #os.system("post.py project_idrfpm_semfem/results/post.res")	#brings up thousands of plots
  
  #checkpoint.append(time.time())
  ####################################
  # Calculate flux density harmonics #
  ####################################
  if enable_flux_density_harmonic_calc == 1:
    M = 1024					#M should be even number to optimally use the FFT algorithm
    fplfile = dir_parent+"project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl"
    Br = []
    Br.append([])
    Br.append([])
    Br.append([])
    #get_arc_B(self, fpl_file, theta1, theta2, r, npoints) #function prototype from semfem.py
    Bm, Br[0], Bt = sf.get_arc_B(fplfile, 0,2*mechRotation, rn - h/2, 2*M)		#the last argument specifies the number of data points one desires
    Bm, Br[1], Bt = sf.get_arc_B(fplfile, 0,2*mechRotation, rn, 2*M)
    Bm, Br[2], Bt = sf.get_arc_B(fplfile, 0,2*mechRotation, rn + h/2, 2*M)

    #get first harmonic of Br @ rn
    pl.figure(3)

    Br_fft = abs(np.fft.rfft(Br[1],M)*2/M)	#bereken fft en normaliseer (gebruik NumPy library) (kan rfft gebruik omdat sein net reel is) (gebruik n/2 punte omdat ons net positiewe frekwensies wil bereken)    
    Br1 = Br_fft[1]
    max_Br_1 = max(Br[1])

    #my_range = 20
    #k = np.arange(my_range)			#maak aanname dat elektriese periode van die sein 1 Hz is (dan sal 1st harmoniek by k = 1 le, en dus verteenwoordig k ook dan sommer die frekwensie in Hz)
    #pl.plot(k,abs(Br_fft[:my_range]))
    #pl.xlabel('Freq (Hz)')
    #pl.ylabel('|$B_{r|r_{n}}$|')
    #pl.show()

    Br_THD = sqrt(sum(Br_fft[2:(M-1)/2]**2))/Br1
    

  #checkpoint.append(time.time()) 
  #############################
  # Calculate general resutls #
  #############################
  if enable_general_calc == 1:
    fld = zeros(steps)
    flq = zeros(steps)
    torque_dq = zeros(steps)
    P_wf = 0

    #checkpoint.append(time.time())
    #CALCULATE DQ VOLTAGES
    if steps != 1:
      sfm_post = SEMFEM_postfile(dir_parent+'project_idrfpm_semfem/results/post.res', 1.0/f_e)
      vd = zeros(steps)
      vq = zeros(steps)      
      for i in range(steps):
	vd[i], vq[i] = sf.abc_dq(sfm_post.voltage[0,i], sfm_post.voltage[1,i], sfm_post.voltage[2,i], i/steps*2.*pi)    
    #####################

    #checkpoint.append(time.time())
    #CALCULATE TORQUE
    for i in range(steps):
      fld[i], flq[i] = sf.abc_dq(sf.semfem_flink_vec[i,0], sf.semfem_flink_vec[i,1], sf.semfem_flink_vec[i,2], i/steps*2.*pi)
      torque_dq[i] = -(3/2)*p*(fld[i]*iq - flq[i]*id)
      
    savetxt(dir_parent+'script_results/idrfpm/torque_dq.csv', torque_dq, delimiter = ',',newline='\n',header='torque_dq',fmt='%f')    
    average_torque_dq = average(torque_dq)

    if active_parts != MAGNETS_ONLY and steps != 1:
      average_torque = average(sf.semfem_torque_vec[:,1])
    else:
      average_torque = -1    
    #####################
    
    #checkpoint.append(time.time())
    #CALCULATE MAGNET AND COPPER MASS
    magnet_mass = sf.get_magnet_mass(m, sf.NDFEB_DENSITY)
    copper_mass = 0.
    for i in range(1, 4):
	coil_mass = sf.get_mass(m, i, sf.COPPER_DENSITY)
	copper_mass = copper_mass + coil_mass*sf.semfem_ff_vec[i-1]
	coil_mass = sf.get_mass(m, -i, sf.COPPER_DENSITY)
	copper_mass = copper_mass + coil_mass*sf.semfem_ff_vec[i-1]
    #####################

    #DETERMINE MAX INPUT CURRENT
    if steps != 1:
      max_input_phase_current = max(sf.semfem_i_vec[:,0])
    else:
      max_input_phase_current = iq
  
    '''
    NB: Instantaneous power word bereken, menend die stroom op daardie oomblik word gemaal met die spanning op daardie oomblik. Hierdie stroom en spanning kan inderdaad uit fase wees, ons maal maar net hul waardes waarokal hulle op hul
    eie kurwe sit. Dan, as ons die gemiddeld van hierdie oombliklike drywing neem, dan kry ons P (reele drywing). Daarna kan ons S bereken, deur die RMS waardes vnan stroom en spanning met mekaar te maal (dus nie inagneem of hulle uit
    fase is of nie). Dan kan ons p.f. bereken nou dat ons P en S het.
    '''
    
    #checkpoint.append(time.time())
    #CALCULATE VOLTAGE, S,P,Q AND POWER FACTOR
    Ia_instantaneous = sf.semfem_i_vec[:,0]
    ewf = (pi*end_winding_radius)/l		#end winding factor
    Ra = 0#m.rcoil[0]*(1+ewf)			#phase resistance of coil winding. it is a mesh attribute
    Ia_rms = iq/sqrt(2)    
    
    if steps != 1:
      max_induced_phase_voltage = max(sfm_post.voltage[0])
      max_input_phase_voltage = max_induced_phase_voltage + Ia_rms*sqrt(2)*Ra
      Va_instantaneous = sfm_post.voltage[0]
      P_instantanious = zeros(steps)
      
      for i in range(steps):
	  P_instantanious[i] = Va_instantaneous[i]*Ia_instantaneous[i]
	  
      P = abs(mean(3*P_instantanious))
      
      V_rms=sqrt(mean(Va_instantaneous**2))
      I_rms=sqrt(mean(Ia_instantaneous**2))
      #fl_rms=sqrt(mean(fla**2))
      S= 3*(V_rms*I_rms)   
      
    else:     
      #vd = Ra*id - 2*pi*f_e*flq[0]
      #vq = Ra*iq - 2*pi*f_e*fld[0]
      vd = -2*pi*f_e*flq[0]
      vq = -2*pi*f_e*fld[0]      
      #P = (3/2)*(vd*id + vq*iq) 		#inlcudes P_out and P_conductive (does not include other losses)
      back_emf_rms = sqrt((vd**2 + vq**2)/2)		#that means that this Va_rms, would need to be slightly higher if for instance eddy current losses were included (however eddy current losses are present even when Ia = 0, meaning it should rather be modeled 
	
      if enable_current_density == True:
	turnsPerCoil = int(peak_voltage_allowed/(back_emf_rms*sqrt(2)))	#get integer number which is rounded down so as to not exceed voltage design limit
	area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
	max_induced_phase_voltage = turnsPerCoil*back_emf_rms*sqrt(2)
	max_input_phase_current = iq/turnsPerCoil
	if debug == 1:
	  print("\n(0) Wire_resistivity = %f [Ohm.m]  @ T = %d [Celsius Deg]"%(wire_resistivity,coil_temperature))
	  print("(0) Maximum allowed peak voltage for this design V_a(ln) = %f [V]"%peak_voltage_allowed)
	  print("(0) Peak voltage induced in 1 turn V_a(ln) = %f [V/t]"%(back_emf_rms*sqrt(2)))
	  print("\n(1) Maximum number of turns allowed N = %d turns"%turnsPerCoil)
	  print("(1) Peak voltage induced by all turns V_a(ln) = %f [V]"%max_induced_phase_voltage)
	  print("(1) Peak input phase current Ia = %f [A]"%max_input_phase_current)	
		
    if steps == 1:
      #CALCULATE I^2R CONDUCTION LOSSES (ANALYTICAL)
      wire_resistivity = wire_resistivity*(1+0.0039*(coil_temperature-20))  
      l_coil = 2*(l+(pi*end_winding_radius))*turnsPerCoil
      l_total = (coilsTotal/3)*l_coil
      Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)
      max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical
      if debug == 1:
	print("(1) Ra_analytical = %f [Ohm]"%Ra_analytical)
	print("(1) Peak system input phase voltage V_a(ln) = %f [V]"%max_input_phase_voltage)
    
      #CHECK that max_input_phase_voltage is small enough, adjust turns until limits are adhered to
      
      if enable_manual_turnsPerCoil_limit == 1:
	turnsPerCoil = int(input_data[20])
	area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
	max_induced_phase_voltage = turnsPerCoil*back_emf_rms*sqrt(2)
	max_input_phase_current = iq/turnsPerCoil
	l_coil = 2*(l+(pi*end_winding_radius))*turnsPerCoil
	l_total = (coilsTotal/3)*l_coil
	Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)

	max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical      
      else:   
	
	while max_input_phase_voltage > peak_voltage_allowed:
	  turnsPerCoil = turnsPerCoil - 1
	  
	  area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
	  max_induced_phase_voltage = turnsPerCoil*back_emf_rms*sqrt(2)
	  max_input_phase_current = iq/turnsPerCoil

	  l_coil = 2*(l+(pi*end_winding_radius))*turnsPerCoil
	  l_total = (coilsTotal/3)*l_coil

	  Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)
	  max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical
	  
	if debug == 1:  
	  print("\n(2) Maximum number of turns allowed N = %d turns"%turnsPerCoil)
	  print("(2) Peak voltage induced by all turns V_a(ln) = %f [V]"%max_induced_phase_voltage)
	  print("(2) Peak input phase current Ia = %f [A]"%max_input_phase_current)
	  print("(2) Ra_analytical = %f [Ohm]"%Ra_analytical)
	  print("(2) Peak system input phase voltage V_a(ln) = %f [V]"%max_input_phase_voltage)	  
	
	#if the available area per turn is not adequate to use the finest type of Litz wire (0.2mm with 70 strands), then the turns per coil need to be reduced. If the available area is more than required by this Litz wire, then we can use thicker wire, or use more parallel strands, or insert another turn during manufacturing
	if enable_litz_wire_selection == 1:
	  while area_available_per_turn < single_turn_area*1e6:
	    turnsPerCoil = turnsPerCoil - 1
	  
	    area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
	    max_induced_phase_voltage = turnsPerCoil*back_emf_rms*sqrt(2)
	    max_input_phase_current = iq/turnsPerCoil	

	    l_coil = 2*(l+(pi*end_winding_radius))*turnsPerCoil
	    l_total = (coilsTotal/3)*l_coil

	    Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)
	    max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical
      
	#introduce contingency for 1 less turn during construction, thereby reducing the voltage.
	if enable_voltage_leeway == 1:
	  turnsPerCoil = turnsPerCoil - 1
	
	  area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
	  max_induced_phase_voltage = turnsPerCoil*back_emf_rms*sqrt(2)
	  max_input_phase_current = iq/turnsPerCoil	

	  l_coil = 2*(l+(pi*end_winding_radius))*turnsPerCoil
	  l_total = (coilsTotal/3)*l_coil

	  Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)
	  max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical	
      
	  if debug == 1:
	    print("\n(3) Maximum number of turns allowed N = %d turns"%turnsPerCoil)
	    print("(3) Peak voltage induced by all turns V_a(ln) = %f [V]"%max_induced_phase_voltage)
	    print("(3) Peak input phase current Ia = %f [A]"%max_input_phase_current)
	    print("(3) Ra_analytical = %f [Ohm]"%Ra_analytical)
	    print("(3) Peak system input phase voltage V_a(ln) = %f [V]"%max_input_phase_voltage)       
      
      current_through_slot = max_input_phase_current*turnsPerCoil
      P_conductive_analytical = (3/2)*(max_input_phase_current**2)*Ra_analytical
      if debug == 1:
	print("(3) Total analytical conductive (copper) losses P_cu = %f [W]\n"%(P_conductive_analytical)) 
      
      #S = 3*Va_rms*Ia_rms
      S = (3/2)*max_input_phase_voltage*max_input_phase_current
      P = (3/2)*(vd*id + vq*iq)  + P_conductive_analytical
      #P = (3/2)*(vd*id + vq*iq) 		#inlcudes P_out and P_conductive (does not include other losses)
      Q= sqrt(S**2 - P**2)
      if S !=0:
	  power_factor=(P/S)*100
      else:
	  power_factor=0
      #####################   

  checkpoint.append(time.time())
  #################################
  # Calculate eddy current losses #
  #################################
  #As of yet, eddy current losses only take into account Br,Bt, harmonics, and variation of Br and Bt at various positions in coil height
  if enable_eddy_losses_calc == 1:
    pretty_eddy = PrettyTable(['Eddy Current Variable','Value','Unit'])
    pretty_eddy.align = 'l'
    pretty_eddy.border= True  
    #pretty_eddy.add_row(["INPUTS",0,""])
    pretty_eddy.add_row(["INPUTS","------------","------------"])
    pretty_eddy.add_row(["Turns per coil per phase (N)",turnsPerCoil,"turns"])
    pretty_eddy.add_row(["Total number of stator coils (Q)",coilsTotal,""])
    pretty_eddy.add_row(["Number of parallel strands (nc)",parallel_stranded_conductors,""])
    pretty_eddy.add_row(["Active conductor length",l,"[m]"])
    pretty_eddy.add_row(["Wire diameter",wire_diameter,"[m]"])
    pretty_eddy.add_row(["Peak fundamental flux density (B_1_pk)",Br,"[T]"])
    pretty_eddy.add_row(["Fundamental electrical angular frequency (We)",2*pi*f_e,"[rad/s]"])
    wire_resistivity = 1.7e-8
    pretty_eddy.add_row(["Copper resistivity (p)",wire_resistivity,"[Ohm.m]"])
    
    M = 1024
    fplfile = dir_parent+"project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl"
    layer_range = linspace(ri+hmi_r+gi, ri+hmi_r+gi+hc, num=number_of_layers_eddy, endpoint=True)
    Bm, Br_middle,Bt_middle = sf.get_arc_B(fplfile, 0,2*mechRotation, ri+hmi_r+gi+(0.5*hc), 2*M) #Br_middle is the Instantaneous (or rather spacial) value of Br_middle. Br_middle can thus be used to draw sinusoidal-like curve.
    Br_middle_fft = abs(np.fft.rfft(Br_middle,M)*2/M) #get the peak values of all the harmonics
    Bt_middle_fft = abs(np.fft.rfft(Bt_middle,M)*2/M) #get the peak values of all the harmonics
    print("Br_middle_fft[1] = %f"%Br_middle_fft[1])
    print("Bt_middle_fft[1] = %f"%Bt_middle_fft[1])
    
    #print "layer range"
    #print layer_range
    #print "NQa"
    NQa = turnsPerCoil * coilsTotal * parallel_stranded_conductors
    #print(NQa)
    #wire_resistivity = 1.678e-8 #the larger the wire resistivity, the smaller the eddy currents, and thus less losses
    #thus heat is good for reducing eddy current losses
    piLd_div_16rho = (pi*l*(wire_diameter**4))/(16*wire_resistivity)
    #print(pi)
    #print(l)
    #print(wire_diameter**4)
    #print(wire_resistivity)
    #print(piLd_div_16rho)
    
    pretty_layer = PrettyTable(['Layer number','Br_layer_fft(max)','Bt_layer_fft(max)'])#,'Layer 3','Layer 4','Layer 5'])
    pretty_layer.align = 'l'
    pretty_layer.border= True      
    
    
    P_eddy = 0
    number_of_conductors_in_layer = (2*turnsPerCoil*parallel_stranded_conductors)/number_of_layers_eddy		#the factor 2 is inserted because each turn has a wire facing the upward axial direction and a wire facing the downward axial direction within a single layer
														#this is also why Stegmann's 2011 paper had a divide by 16 factor instead of a 32
    total = 0
    for j in range (1, number_of_layers_eddy+1): #for each layer
      Bm, Br_layer,Bt_layer = sf.get_arc_B(fplfile, 0,2*mechRotation, layer_range[j-1], 2*M)
      #print max(Bt_layer)
      #print("\nlayer number = %d"%j)
      Br_layer_fft = abs(np.fft.rfft(Br_layer,M)*2/M)
      Bt_layer_fft = abs(np.fft.rfft(Bt_layer,M)*2/M)
      #print("Br_layer_fft(max) = %f"%max(Br_layer_fft))
      #print("Bt_layer_fft(max) = %f"%max(Bt_layer_fft))
      
      pretty_layer.add_row(['%d'%j,max(Br_layer_fft),max(Bt_layer_fft)])
      
      #print("layer = %d"%j)
      for i in range (1, max_harmonics_eddy+1):  #for each harmonic
	#print("harmonic number = %d"%i)
	#print "P_eddy_temp"
	#print piLd_div_16rho*(i**2)*(Br_layer_fft[i]**2 + Bt_layer_fft[i]**2)
	squared_sum = (Br_layer_fft[i]**2 + Bt_layer_fft[i]**2)
	#print("squared_sum = %f"%squared_sum)
	squared_sum_times_squared_freq = ((2*pi*f_e*i)**2)*squared_sum
	#print("squared_sum_times_squared_freq = %f"%squared_sum_times_squared_freq)
	times_everything_else = (NQa/number_of_layers_eddy) * piLd_div_16rho * squared_sum_times_squared_freq
	#print("Power dissipated by this harmonic and this layer = %f [W]"%times_everything_else)
	total = total + times_everything_else
	P_eddy = P_eddy + (i**2)*(Br_layer_fft[i]**2 + Bt_layer_fft[i]**2)

    print("Total eddy losses = %f [W]"%total)
  
    P_eddy = coilsTotal * number_of_conductors_in_layer * ((pi*l*(wire_diameter**4)*(2*pi*f_e)**2)/(32*wire_resistivity)) * P_eddy
    pretty_eddy.add_row(["OUTPUTS","------------","------------"])
    
    p_eddy_1 = (2*turnsPerCoil * coilsTotal * parallel_stranded_conductors * pi * l * (wire_diameter**4) * ((2*pi*f_e)**2)*(Br_middle_fft[1]**2))/(wire_resistivity)
    
    pretty_eddy.add_row(["p_eddy_1 (no harmonics, only radial flux, no layers)/16",p_eddy_1/16,"[W]"])
    pretty_eddy.add_row(["p_eddy_1 (no harmonics, only radial flux, no layers)/32",p_eddy_1/32,"[W]"])
    pretty_eddy.add_row(["p_eddy_1 (no harmonics, only radial flux, no layers)/64",p_eddy_1/64,"[W]"])
    pretty_eddy.add_row(["p_eddy_1 (no harmonics, only radial flux, no layers)/128",p_eddy_1/128,"[W]"])
    pretty_eddy.add_row(["------------","------------","------------"])
    
    p_eddy_2 = (2*turnsPerCoil * coilsTotal * parallel_stranded_conductors * pi * l * (wire_diameter**4) * ((2*pi*f_e)**2)*(Br_middle_fft[1]**2 + Bt_middle_fft[1]**2))/(wire_resistivity)
    
    pretty_eddy.add_row(["p_eddy_2 (no harmonics, no layers)/16",p_eddy_2/16,"[W]"])
    pretty_eddy.add_row(["p_eddy_2 (no harmonics, no layers)/32",p_eddy_2/32,"[W]"])
    pretty_eddy.add_row(["p_eddy_2 (no harmonics, no layers)/64",p_eddy_2/64,"[W]"])
    pretty_eddy.add_row(["p_eddy_2 (no harmonics, no layers)/128",p_eddy_2/128,"[W]"])    
    pretty_eddy.add_row(["------------","------------","------------"])
    
    squared_sum = 0
    for i in range (1, max_harmonics_eddy+1):
      squared_sum = squared_sum + (i**2)*(Br_middle_fft[i]**2 + Bt_middle_fft[i]**2)
      print("Br_middle_fft[%d] = %f"%(i,Br_middle_fft[i]))
      print("Bt_middle_fft[%d] = %f"%(i,Bt_middle_fft[i]))
    
    p_eddy_3 = (2*turnsPerCoil * coilsTotal * parallel_stranded_conductors * pi * l * (wire_diameter**4) * ((2*pi*f_e)**2)*squared_sum)/(wire_resistivity)
    
    pretty_eddy.add_row(["p_eddy_3 (%d harmonics, no layers)/16"%(max_harmonics_eddy),p_eddy_3/16,"[W]"])
    pretty_eddy.add_row(["p_eddy_3 (%d harmonics, no layers)/32"%(max_harmonics_eddy),p_eddy_3/32,"[W]"])
    pretty_eddy.add_row(["p_eddy_3 (%d harmonics, no layers)/64"%(max_harmonics_eddy),p_eddy_3/64,"[W]"])
    pretty_eddy.add_row(["p_eddy_3 (%d harmonics, no layers)/128"%(max_harmonics_eddy),p_eddy_3/128,"[W]"])    
    pretty_eddy.add_row(["------------","------------","------------"])
    
    total = 0
    layer_range = linspace(ri+hmi_r+gi, ri+hmi_r+gi+hc, num=number_of_layers_eddy, endpoint=True)
    conductors_per_layer = (2*turnsPerCoil*coilsTotal*parallel_stranded_conductors)/number_of_layers_eddy
    p_eddy_4 = 0
    for j in range (1, number_of_layers_eddy+1): #for each layer
      Bm, Br_layer,Bt_layer = sf.get_arc_B(fplfile, 0,2*mechRotation, layer_range[j-1], 2*M)
      Br_layer_fft = abs(np.fft.rfft(Br_layer,M)*2/M)
      Bt_layer_fft = abs(np.fft.rfft(Bt_layer,M)*2/M)
      
      squared_sum = 0
      for i in range (1, max_harmonics_eddy+1):  #for each harmonic
	squared_sum = squared_sum + (i**2)*(Br_layer_fft[i]**2 + Bt_layer_fft[i]**2)

      p_eddy_4 = conductors_per_layer * squared_sum

    p_eddy_4 = (p_eddy_4 * pi * l * (wire_diameter**4) * ((2*pi*f_e)**2))/(wire_resistivity)

    pretty_eddy.add_row(["p_eddy_4 (%d harmonics, %d layers)/16"%(max_harmonics_eddy,number_of_layers_eddy),p_eddy_4/16,"[W]"])
    pretty_eddy.add_row(["p_eddy_4 (%d harmonics, %d layers)/32"%(max_harmonics_eddy,number_of_layers_eddy),p_eddy_4/32,"[W]"])
    pretty_eddy.add_row(["p_eddy_4 (%d harmonics, %d layers)/64"%(max_harmonics_eddy,number_of_layers_eddy),p_eddy_4/64,"[W]"])
    pretty_eddy.add_row(["p_eddy_4 (%d harmonics, %d layers)/128"%(max_harmonics_eddy,number_of_layers_eddy),p_eddy_4/128,"[W]"])    
    pretty_eddy.add_row(["------------","------------","------------"])    
    
  else:
    P_eddy = 0
    
  
    
  if enable_general_calc == 1:
    checkpoint.append(time.time())
    #CALCULATE I^2R CONDUCTION LOSSES (SEMFEM)
    if steps != 1:
      P_conductive_semfem=3*sf.semfem_copper_loss_vec[:,0]	#multiplying with 3, assuming that each phase has the exact same copper losses
      P_conductive_semfem=sum(P_conductive_semfem)/steps
      P_core_loss=sum(sf.semfem_avg_core_loss_vec)
    else:
      P_conductive_semfem = sf.semfem_copper_loss_vec[0,0] + sf.semfem_copper_loss_vec[0,1] + sf.semfem_copper_loss_vec[0,2]	#power of each phase is added. 3phase power is constant.
      P_core_loss=0
      
    P_conductive = P_conductive_semfem*(1+ewf)	#scale the losses calculated by SEMFEM using only the stack_length  
    #####################   
    
    checkpoint.append(time.time())
    #CALCULATE EFFICIENCY
    #machine_speed = (60*f_e)/p # 60*f_e/Qr   (Qr = 10)		#should be 290rpm
    P_out = (n_rpm*(pi/30))*average_torque_dq
    if steps == 1:
      P_losses = P_conductive_analytical + P_core_loss + P_eddy + P_wf
    else:
      P_losses = P_conductive_semfem* + P_core_loss + P_eddy + P_wf
    P_in = P_out + P_losses
    
    if P_in != 0:
	efficiency = (P_out/P_in) * 100
    else:
	efficiency = 0
    #####################      
  
  ############################
  # Output data to CSV files #
  ############################
  
  checkpoint.append(time.time())

  if enable_flux_density_harmonic_calc == 1:
    savetxt(dir_parent+'script_results/idrfpm/br.csv', transpose([Br[0],Br[1],Br[2]]), delimiter = ',',newline='\n',header='Br @(rn-h/2), Br @(rn), Br @(rn+h/2)',fmt='%f')
  else:
    max_Br_1=0
    Br1=0
    Br_THD=0
    
  if active_parts != MAGNETS_ONLY and steps != 1:
    savetxt(dir_parent+'script_results/idrfpm/voltage.csv', transpose([sfm_post.voltage[0],sfm_post.voltage[1],sfm_post.voltage[2]]), delimiter = ',',newline='\n',header='Va, Vb, Vc',fmt='%f')

      
  checkpoint.append(time.time())
  #this section should only execute if optimization_output.csv already exists and we just want to APPEND to the file
  if enable_optimization_csv_output == 1:
    with open(dir_parent+'script_results/idrfpm/optimization_output.csv',"a") as opt_output_file:
      if active_parts != MAGNETS_ONLY and steps != 1:
	opt_output_file.write('%g, %g, %g, %g, %g, %g, %g ,%g ,%g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g\n'%(p, kq, l, g, hc, kmo, Brem, muR, f_e, id, iq, number_of_layers_eddy, max_harmonics_eddy, wire_diameter, parallel_stranded_conductors, wire_resistivity, active_parts, turnsPerCoil, fill_factor,max_Br_1,Br1,Br_THD*100,P_eddy,magnet_mass,copper_mass,P_core_loss,max_input_phase_current,max_input_phase_voltage,max_induced_phase_voltage,max(sf.semfem_flink_vec[:,0]),efficiency,average_torque_dq,P,P_out,P_losses,coil_side_area))
      else:
	opt_output_file.write('%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g\n'%(p,kq,l,g,hmi_r,hc,hmo_r,kmo,kc,ri,ro,number_of_layers_eddy,wire_diameter,parallel_stranded_conductors,turnsPerCoil,fill_factor,J,max_input_phase_current,max_induced_phase_voltage,max_input_phase_voltage,average_torque_dq,magnet_mass,copper_mass,Ra_analytical,efficiency,S,Q,P,P_out,P_conductive_analytical,P_eddy,P_wf,P_core_loss,P_losses,power_factor,coil_side_area,max_Br_1,Br1,Br_THD*100))

  checkpoint.append(time.time())
  #################
  # Print results #
  #################
  if enable_terminal_output == 1:
    if enable_general_calc == 1 and active_parts != MAGNETS_ONLY:# and steps != 1:
      addToTable("Max input phase current", max_input_phase_current, "[A]", True,dir_parent)
      addToTable("Max induced phase voltage",max_induced_phase_voltage, "[V]", True,dir_parent) 	#phase A max induced voltage
      addToTable("Max input phase voltage",max_input_phase_voltage, "[V]", True,dir_parent) 	#phase A max induced voltage
      addToTable("Max flux linkage",max(sf.semfem_flink_vec[:,0]), "[Wb]", True,dir_parent)
      addToTable("Average torque produced (semfem)",average_torque,"[Nm]", steps != 1,dir_parent)
      addToTable("*********Average torque produced (dq quantities)********",average_torque_dq,"[Nm]", True,dir_parent)
      addToTable("*******************Magnet mass**************************",magnet_mass,"[kg]", True,dir_parent)
      addToTable("Copper mass",copper_mass,"[kg]", True,dir_parent)
      addToTable("Phase resistance",Ra,"[Ohm]", True ,dir_parent)
      if steps == 1:
	addToTable("Phase resistance (analytical)",Ra_analytical,"[Ohm]", True,dir_parent)
      #addToTable("Cogging torque",cogging,"[Nm]", True,dir_parent)
      addToTable("Electrical frequency",f_e,"[Hz]", True,dir_parent)
      addToTable("Machine speed",n_rpm,"[rpm]", True,dir_parent)
      addToTable("**********************Efficiency************************",efficiency,"[%]", True,dir_parent)
      addToTable("Output Torque/Mass Ratio",(average_torque_dq/(magnet_mass+copper_mass)),"[Nm/kg]", True,dir_parent)
      addToTable("Output Torque/PM Mass Ratio",(average_torque_dq/magnet_mass),"[Nm/kg]", True,dir_parent)
      addToTable("Input Apparant Power (S)",S,"[VA]", True,dir_parent)
      if steps == 1:
	addToTable("Input Reactive Power (Q)",Q,"[VAR]", True,dir_parent)
      addToTable("Input Real Power (mean value of instantaneous power over full period) (P)",P,"[W]", steps != 1,dir_parent)
      addToTable("Input Real Power (P_in)",P_in,"[W]", True,dir_parent)
      addToTable("Input Real Power (P)",P,"[W]", True,dir_parent)
      addToTable("Output Mechanical Power",P_out,"[W]", True,dir_parent)
      if steps == 1:
	addToTable("Conductive loss (analytical with end-windings)",P_conductive_analytical,"[W]", True,dir_parent)
      addToTable("Conductive loss (SEMFEM)",P_conductive_semfem,"[W]", True,dir_parent)
      addToTable("Conductive loss (with end-windings)",P_conductive,"[W]", True,dir_parent)
      addToTable('Eddy current losses',P_eddy,'[W]', enable_eddy_losses_calc == 1,dir_parent)
      addToTable("Wind & Friction losses",P_wf,"[W]", True,dir_parent)
      addToTable("Core loss",P_core_loss,"[W]", True,dir_parent)					#core loss will be zero because there is no B-H hysteresis effect for an ironless machine  
      addToTable("Total losses",P_losses,"[W]", True,dir_parent)
      if steps == 1:
	addToTable("Power factor",power_factor,"[%]", True,dir_parent)
      addToTable("Fill factor",fill_factor,"[p.u.]",True,dir_parent)
      addToTable("Coil side area",coil_side_area*1000**2,"[mm^2]",True,dir_parent)
      if steps == 1:
	addToTable("Area available per Turn (including fill factor)",area_available_per_turn,"[mm^2]",True,dir_parent)
      addToTable("Area required per Turn (Litz with parallel strands)",single_turn_area*1000**2,"[mm^2]",True,dir_parent)
      addToTable("Turns per coil (N)",turnsPerCoil,"[Turns]",True,dir_parent)
      addToTable("Current through single slot (peak)",current_through_slot,"[A]",True,dir_parent)
      addToTable("Current density (peak)",J,"[A/mm^2]",True,dir_parent)
      addToTable("Coil temperature",coil_temperature,"[Deg. C]",True,dir_parent)
      if steps == 1:
	addToTable("Coil winding length",l_coil,"[m]",True,dir_parent)
	addToTable("Phase winding length",l_total,"[m]",True,dir_parent)
      addToTable("Copper resistivity",wire_resistivity,"[Ohm.m]",True,dir_parent)
      
      #addToTable("Active slot area",active_slot_area*1000**2,"[mm^2]",True,dir_parent)
      #addToTable("Achieved fill factor",active_slot_area/coil_side_area,"[p.u.]",True,dir_parent)
         
    else:
      addToTable("Estimated torque produced (dq quantities)",average_torque_dq,"[Nm]", True,dir_parent)
      
    
    if enable_flux_density_harmonic_calc == 1:
      addToTable('Brmax|rn',max_Br_1,'[T]', True,dir_parent)
      addToTable('Br1|rn',Br1,'[T]', True,dir_parent)
      addToTable('THD(Br|rn)',(Br_THD*100),'[%]', True,dir_parent)


  if enable_terminal_output == 1:
    save_stdout = sys.stdout
    sys.stdout = open(dir_parent+"pretty_terminal_table.txt", "wb")
    print pretty_terminal_table
    sys.stdout = save_stdout    
    
    if VERBOSITY == 1:
      print pretty_terminal_table
      if enable_eddy_losses_calc == 1:
	print pretty_eddy
	print pretty_layer
	pretty_eddy.clear_rows()
	pretty_layer.clear_rows()
  
    pretty_terminal_table.clear_rows()

    #pretty_terminal_table = None
  

  #os.system("post.py --voltage project_idrfpm_semfem/results/post.res -f %f" % (f_e))	#brings up all the voltage plots, for more usage and options to use this command, see
											      #SEMFEM_git/SEMFEM/core/plotter/post.py
								  
  #os.system("post.py project_idrfpm_semfem/results/post.res -f %f" % (f_e))  
  

  checkpoint.append(time.time())  

  sf.semfem_end()
  
  #if enable_show_iteration_number == True:
    #sys.stdout.write("SEMFEM simulations executed: %4d\r" %(iteration_number))
    #print("SEMFEM simulations executed: %4d\r" %(iteration_number))
  
  
  with open(dir_parent+'script_results/main/running_flags.csv', 'wb') as running_flags_file:
    running_flags_file.write("iteration_number,%d"%(iteration_number+1)) 
  
  return 1


if __name__ == '__main__': #when executed by itself
  main('input_original.csv')
