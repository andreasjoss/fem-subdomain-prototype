Trying to replicate exact same results from FEM and subdomain analysis.

Pay attention to:
1. Use same number of turns
2. Use same phase currents
3. Compare semfem dq torque vs step-average torque
4. voltage leeway enabled or not (will remove a turn)
5. Use sub-air gaps in semfem or not
6. eddy current losses included or not
7. litz-wire selection enabled or not
8. use same wire resistivity (check end value, after modified with new temperature)
9. check if efficiency is calculated similary
10. Rs resistance, how it is implemented